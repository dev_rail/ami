<?php


namespace frontend\controllers;

use frontend\models\order\OrderLink;
use yii\web\NotFoundHttpException;

class LController extends BaseController
{
    public function actionIndex($id)
    {
        $order_link = $this->findModel($id);

        if (!$order_link)
            return $this->redirect('/');

        $customer = $order_link->order->orderCustomer->customer;
        $customer->auth();

        $this->redirect(['order/v', 'id' => $order_link->order_id]);
    }

    public function actionR($id){
        $order_link = $this->findModel($id);

        if (!$order_link)
            return $this->redirect('/');

        $customer = $order_link->order->orderCustomer->customer;
        $customer->auth();

        $this->redirect(['order/raiting', 'id' => $order_link->order_id]);
    }

    public function actionGetOrder($id){
        $order_link = $this->findModel($id);

        if ($order_link)
            $order_link->order->setCooking();

        return ['result' => 'ok'];
    }

    private function findModel($code)
    {
        $model = OrderLink::findOne(['code' => $code]);

        if (!$model)
            return false;

        return $model;
    }
}
