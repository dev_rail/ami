<?php


namespace frontend\controllers;


use common\models\order\OrderPay;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;

class PaymentController extends Controller
{
    public function actionIndex(){
        $source = file_get_contents('php://input');
        $params = json_decode($source, true);

        if ($params["event"] == "payment.succeeded") {
            $order_pay = OrderPay::findOne(['pay_id' => ArrayHelper::getValue($params, "object.id")]);
            if ($order_pay && $order_pay->order)
                $order_pay->order->getPaidOnline();
        }

        return [];
    }
}