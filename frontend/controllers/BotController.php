<?php

namespace frontend\controllers;


use frontend\models\order\Order;
use Yii;
use yii\debug\models\search\Log;
use yii\helpers\Html;
use yii\rest\Controller;
use yii\web\Response;

class BotController extends Controller
{
    public function actionIndex(){
        $data = Yii::$app->request->getRawBody();
        $data = json_decode($data);

        if (isset($data->callback_query) && isset($data->callback_query->data)){
            $dt_array = explode("-", $data->callback_query->data);

            $t_user = $data->callback_query->from;
            $from = $t_user->first_name;
            if (isset($t_user->last_name))
                $from .= " ".$t_user->last_name;

            if (isset($t_user->username))
                $from .= " (".$t_user->username.")";

            if (count($dt_array) == 2)
                $this->runMessage($dt_array[1], $dt_array[0], $from);
        }

        return ['result' => 'OK'];
    }

    public function runMessage($id, $operation, $from = null){
        $order = Order::findOne($id);

        if (!$order)
            return;

        switch ($operation){
            case "job":
                $order->setCooking($from);
                break;
            case "delivery":
                $order->setDelivering($from);
                break;
            case "finish":
                $order->setFinish($from);
                break;
            case "cancel":
                $order->setCancel($from);
                break;
        }
    }

    public function actionNavigate($lat = false, $lon = false){
        Yii::$app->response->format = Response::FORMAT_HTML;

        if ($lat == false or $lon == false)
            return "Невозможно сформировать маршрут!";

        return Html::a('Открыть Яндекс.Навигатор', "yandexnavi://build_route_on_map?lat_to="
                    . $lat . "&lon_to=" . $lon, ['style' => 'font-size: 40px;']);
    }
}