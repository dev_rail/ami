<?php

namespace frontend\controllers;


use frontend\models\customer\CustomerAddresses;

class AjaxController extends RestBaseController
{
    public function actionAddress($id){
        $customer_address = CustomerAddresses::findOne($id);

        if (!$customer_address)
            return ['street' => '', 'home' => '', 'place' => ''];

        return ['street' => $customer_address->street, 'home' => $customer_address->home, 'place' => $customer_address->place];
    }
}