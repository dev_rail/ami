<?php


namespace frontend\controllers;


use common\models\order\OrderPay;
use frontend\components\OnlinePay;
use frontend\components\YandexPay;
use frontend\models\order\AddressOrderModel;
use frontend\models\order\Order;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;


class OrderController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['address'],
                'rules' => [
                    [
                        'actions' => ['address'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }


    public function actionCart()
    {
        $this->bodyParams = "woocommerce-cart";
        $items = \Yii::$app->cart->getAllItemsQuery()->all();

        if (Yii::$app->request->post('promo'))
            Yii::$app->promo->findPromo(Yii::$app->request->post('promo'));

        if (Yii::$app->request->post('resetPromo'))
            Yii::$app->promo->reset();

        if (count($items) == 0)
            return $this->render('no-cart');

        return $this->render('cart', ['items' => $items]);
    }

    public function actionPhone()
    {
        if (!\Yii::$app->user->isGuest)
            $this->redirect(['/order/address']);

        if (\Yii::$app->request->isPost)
        {
            $phone = \Yii::$app->request->post('phone');
            \Yii::$app->phoneConfirmation->getCodeByPhone($phone);
            $this->redirect(['/order/code']);
        }

        return $this->render('phone');
    }

    public function actionCode()
    {
        $error = false;

        if (\Yii::$app->request->isPost)
        {
            $code = \Yii::$app->request->post('code');
            $result = \Yii::$app->phoneConfirmation->confirmByCode($code);

            if ($result === true)
                $this->redirect('/order/address');

            $error = 'Код несоответствует!';
        }

        return $this->render('code', ['error' => $error]);
    }

    public function actionAddress()
    {
        $this->bodyParams = "woocommerce-checkout";
        $items = \Yii::$app->cart->getAllItemsQuery()->all();

        if (count($items) == 0)
            $this->redirect(['/order/cart']);

        $model = new AddressOrderModel();

        if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post()))
        {
            $order = $model->formOrder();

            if (\Yii::$app->user->identity->backSession){
                \Yii::$app->user->identity->backSession->delete();
                $url = \Yii::$app->params['domens']['crm']."/order/view?id=".$order->id;
                return $this->redirect($url, 301);
            }elseif ($order->type_pay == Order::TYPE_PAY_CASH)
                $this->redirect(['/order/view', 'id' => $order->id]);
            elseif ($order->type_pay == Order::TYPE_PAY_ELECTRON)
                $this->redirect(['/order/pay', 'id' => $order->id]);
        }

        return $this->render('address', ['items' => $items, 'model' => $model]);
    }

    public function actionView($id)
    {
        $this->bodyParams = "woocommerce-order-received woocommerce-checkout";

        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    public function actionV($id)
    {
        $this->bodyParams = "woocommerce-order-received woocommerce-checkout";

        $model = $this->findModel($id);

        return $this->render('v', ['model' => $model]);
    }

    public function actionPay($id)
    {
        $order = $this->findModel($id);
        $pay = new OnlinePay();
        $url = $pay->pay($order);

        $this->redirect($url);
    }

    public function actionRaiting($id){
        $order = $this->findModel($id);

        $model = $order->orderRaiting;

        if (!$model)
            return $this->redirect('/');

        if (Yii::$app->request->isPost){
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->is_processed = true;
                $model->save();
                $order->deleteOrderLink();
                return $this->redirect(['order/thanks']);
            }
        }

        return $this->render('raiting', ['model' => $model]);
    }

    public function actionThanks(){
        return $this->render('thanks');
    }

    private function findModel($id)
    {
        $model = Order::find()
            ->joinWith('orderCustomer')
            ->where(['Orders.id' => $id])
            ->andWhere(['customer_id' => \Yii::$app->user->id])
            ->one();

        if (!$model)
            throw new NotFoundHttpException('Данной страницы не существует!');

        return $model;
    }

    public function actionSberbank($orderId){
        $order_pay = OrderPay::findOne(['pay_id' => $orderId]);

        if (!$order_pay)
            throw new NotFoundHttpException('no orderId');

        $order = $order_pay->order;

        return $this->redirect(['view', 'id' => $order->id]);
    }
}
