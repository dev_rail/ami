<?php


namespace frontend\controllers;


use frontend\models\order\Cart;
use yii\rest\Controller;

class CartController extends RestBaseController
{
    public function actionAdd($good_id, $good_size_id)
    {
        $message = \Yii::$app->cart->add($good_id, $good_size_id);

        return [
            'status' => 'OK',
            'message' => $message
        ];
    }

    public function actionGetText()
    {
        return [
            'status' => 'OK',
            'message' => \Yii::$app->cart->textCart,
            'mobMessage' => \Yii::$app->cart->mobileTextCart
        ];
    }

    public function actionDelete($id)
    {
        \Yii::$app->cart->delete($id);

        return [
            'status' => 'OK'
        ];
    }

    public function actionPlus($cart_id)
    {
        \Yii::$app->cart->plusOne($cart_id);

        return [
            'status' => 'OK'
        ];
    }

    public function actionMinus($cart_id)
    {
        \Yii::$app->cart->minusOne($cart_id);

        return [
            'status' => 'OK'
        ];
    }
}
