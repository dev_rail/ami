<?php


namespace frontend\controllers;

use yii\rest\Controller;
use yii\web\Response;

class RestBaseController extends Controller
{
    public function behaviors()
    {
        $behaviors =  parent::behaviors();

        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function runAction($id, $params=array()){
        $params = array_merge(\Yii::$app->request->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }
}
