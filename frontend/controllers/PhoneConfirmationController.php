<?php


namespace frontend\controllers;


class PhoneConfirmationController extends RestBaseController
{
    public function actionGetCode($phone)
    {
        \Yii::$app->phoneConfirmation->getCodeByPhone($phone);

        return [
            'status' => 'OK'
        ];
    }

    public function actionConfirm($code)
    {
        $result = \Yii::$app->phoneConfirmation->confirmByCode($code);

        if ($result === true)
            $this->redirect('/order/address');

        return [
            'status' => 'error',
            'message' => 'Код несоответствует!'
        ];
    }
}
