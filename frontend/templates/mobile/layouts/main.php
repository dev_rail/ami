<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        .product-weight{
            position: absolute;
            top: 321px;
            right: 95px;
            border: 1px solid #00000057;
            border-radius: 5px;
            padding: 0 10px;
            background-color: white;
        }

        @media (max-width: 986px) { /* это будет показано при разрешении монитора до 930 пикселей */
            .header-block{
                height: 110px;
            }

            .product-weight{
                top: 300px;
            }
        }

        @media (max-width: 930px) { /* это будет показано при разрешении монитора до 930 пикселей */
            .header-block{
                height: 110px;
            }

            .product-weight{
                top: 300px;
            }
        }

        @media (max-width: 768px) { /* если максимальное разрешение экрана составит 469 пикселей */
            .header-block{
                height: 60px;
            }

            .product-weight{
                top: 300px;
            }
        }

        @media (max-width: 469px) { /* если максимальное разрешение экрана составит 469 пикселей */
            .header-block{
                height: 90px;
            }

            .product-weight{
                top: 269px;
            }
        }

        @media (max-width: 320px) { /* если максимальное разрешение экрана составит 469 пикселей */
            .header-block{
                height: 90px;
            }

            .product-weight{
                top: 229px;
            }
        }
    </style>
</head>
<body class="page-template-template-homepage-v2 home-v1 <?=$this->context->bodyParams?>">
<?$this->beginBody()?>
<div id="page" class="hfeed site">
    <header id="masthead" class="site-header header-v1"  style="background-image: none; padding: 0;">
        <div class="col-full header-block">
            <div class="header-wrap">
                <div class="site-branding" style="margin-top: 20px; margin-left: auto; margin-right: auto;">
                    <a href="/" class="custom-logo-link" rel="home">
                        <img alt="logo" src="/assets/images/logo.png" />
                    </a>
                </div>
                <!-- #site-navigation -->
            </div>
        </div>
    </header>
    <div id="content" class="site-content" tabindex="-1">
        <div class="col-full">
            <div class="pizzaro-sorting">
                <div class="food-type-filter">

                </div>
            </div>
            <div id="primary" class="content-area">
                <main id="main" class="site-main" >
                    <?=$content;?>
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
        <!-- .col-full -->
    </div>
    <!-- #content -->
    <footer id="colophon" class="site-footer footer-v1" >
        <div class="col-full">
            <div class="footer-social-icons">
                <span class="social-icon-text">Подпишитесь на нас</span>
                <ul class="social-icons list-unstyled">
                    <li><a class="fa fa-instagram" href="https://www.instagram.com/invites/contact/?i=1ir9mrgdqrwpt&utm_content=7j7wycl"></a></li>
                </ul>
            </div>
            <div class="footer-logo">
                <a href="/" class="custom-logo-link" rel="home">
                    <img alt="logo" src="/assets/images/logo.png" />
                </a>
            </div>
            <div class="site-address">
                <ul class="address">
                    <li>AMIPizza Пиццерия</li>
                    <li>Адрес: г. Казань, ул. Габдуллы Тукая, д. 115 к. 4</li>
                    <li>Телефон: +7 995 330-27-00</li>
                    <li>ИП Мухаметшин Раиль Равилевич</li>
                    <li>ИНН 160501526941</li>
                    <li>ОГРНИП 319169000090678</li>
                    <li>Мы работаем с 10:00 до 22:00</li>
                </ul>
            </div>
            <div class="site-info">
                <p class="copyright">Copyright &copy; 2019 AMIPizza. Все права защищены.</p>
            </div>
            <div class="pizzaro-handheld-footer-bar" style="height: 56px;">
                <ul class="columns-2">
                    <li class="my-account">
                        <a href="tel:+79953302700">Позвонить</a>
                    </li>
                    <li class="cart">
                        <a class="footer-cart-contents" href="<?=\yii\helpers\Url::to(['/order/cart'])?>" title="View your shopping cart" id="mCartCount">
                            <?=Yii::$app->cart->mobileTextCart?>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- .site-info -->
        </div>
        <!-- .col-full -->
    </footer>
    <!-- #colophon -->
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(55201483, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/55201483" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39491547-4">
</script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-39491547-4');
</script>
<?$this->endBody()?>
</body>
</html>
<?php $this->endPage() ?>
