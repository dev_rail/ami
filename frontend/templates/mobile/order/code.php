<?php

/* @var $this \yii\web\View */
$this->title = "Авторизация";

$js = <<<JS
    $(document).ready(function() {
        
    });
JS;

$this->registerJs($js)

?>

<style>
    .formAuth input{
        width: 100%;
        margin-bottom: 20px;
    }

    .formAuth button {
        margin: 0 auto;
        display: block;
    }

    .formText{
        text-align: center;
        width: 100%;
        margin-bottom: 15px;
    }
</style>

<?if($error):?>
    <?=\yii\bootstrap\Alert::widget([
        'options' => [
            'class' => 'alert-warning',
        ],
        'body' => $error
    ])?>
<?endif;?>

<div class="formText">
    <strong>
        Укажите код, который мы выслали Вам в СМС
    </strong>
</div>

<?$form = \yii\widgets\ActiveForm::begin()?>
<div class="formAuth">
    <input type="number" name="code" required>
    <button type="submit">Готово</button>
</div>
<? \yii\widgets\ActiveForm::end()?>

