<?php

/* @var $this \yii\web\View */
$this->title = "Авторизация";

$js = <<<JS
    $(document).ready(function() {
        $(".phone-input").mask("+7 999 999-99-99");
    });
JS;

$this->registerJs($js)

?>

<style>
    .formAuth input{
        width: 100%;
        margin-bottom: 20px;
    }

    .formAuth button {
        margin: 0 auto;
        display: block;
    }

    .formText{
        text-align: center;
        width: 100%;
        margin-bottom: 15px;
    }
</style>

<div class="formText">
    <strong>
        Укажите номер телефона на который отправить СМС со статусом заказа
    </strong>
</div>

<?$form = \yii\widgets\ActiveForm::begin()?>
    <div class="formAuth">
        <input type="tel" class="phone-input" name="phone" required>
        <button type="submit">Готово</button>
    </div>
<? \yii\widgets\ActiveForm::end()?>

