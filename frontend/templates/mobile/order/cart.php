<?php

$this->title = "Корзина Amipizza";

$js = <<<JS
    $(document).on("click", ".mProductClose", function() {
            cart_id = $(this).attr("data-id");
            $.post("/cart/delete", {id:cart_id}, function() {
                $.pjax.reload({container:"#pjax-carts"});
                $.post("/cart/get-text", function(data) {
                    $("#mCartCount").html(data.mobMessage);
                });
            });
        });

        $(document).ready(function() {
          $('[data-toggle="tooltip"]').tooltip();
        });

       $(document).on("click", ".t_icon", function() {
            url = "/cart/";
            
            if ($(this).hasClass("minus"))
                url = url + "minus";
            else if ($(this).hasClass("plus"))
                url = url + "plus";
            
            cart_id = $(this).parent(".touchspin").attr("data-id");
            
            $.post(url, {cart_id:cart_id}, function() {
                $.pjax.reload({container:"#pjax-carts"});
                $.post("/cart/get-text", function(data) {
                    $("#cartText").html(data.mobMessage);
                });
            });
        });
JS;

$this->registerJs($js);
?>

<style>
    .mProduct{
        width: 100%;
        overflow: hidden;
        margin: 0 auto;
        background: #f6f6f6;
        border-radius: 10px;
        margin-bottom: 10px;
        padding-bottom: 20px;
    }

    .mProductName{
        padding: 10px;
        width: 92%;
        overflow: hidden;
        float: left;
    }

    .mProductClose{
        float: right;
        margin-top: 5px;
        margin-right: 5px;
        width: 5%;
    }

    .mProduct2{
        width: 100%;
        float: left;
    }

    .mProductCount{
        width: 40%;
        height: 30px;
        margin-top: 0;
        margin-left: 20px;
        border: 1px solid #c03232;
        border-radius: 5px;
        float: left;
    }

    .touchspin{
        overflow: hidden;
        font-size: 20px;
        margin: 0 auto;
        display: block;
    }

    .touchspin div{
        float: left;
        text-align: center;
        margin: 0 auto;
    }

    .t_icon{
        background: #c03232;
        display: block;
        width: 33%;
    }

    .quantity{
        width: 34%;
        background: white;
    }

    .t_icon a{
        color: white;
        font-weight: bold;
        display: block;
    }

    .mProductPrice{
        float: right;
        margin-right: 20px;
        font-size: 20px;
        font-weight: bold;
    }

    .cartFooter{
        position: fixed;
        width: 100%;
        bottom: 55px;
        height: auto;
        background: #dfdfdf;
        z-index: 1000;
        left: 0;
        padding: 10px;
        display: grid;
    }

    .orderBtn{
        margin: 0 auto;
        display: block;
    }

    .cartFooter{
        font-weight: bold;
        font-size: 22px;
        padding-top: 10px;
    }

    .sum-title{
        float: left;
    }

    .sum-text{
        float: right;
    }
</style>

<div class="promo">
    <?if (Yii::$app->session->hasFlash('error-promo')):?>
        <?=\yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-danger',
            ],
            'body' => Yii::$app->session->getFlash('error-promo')
        ])?>
    <?elseif (Yii::$app->session->hasFlash('success-promo')):?>
        <?=\yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-success',
            ],
            'body' => Yii::$app->session->getFlash('success-promo')
        ])?>
    <?endif;?>
    <?$form = \yii\widgets\ActiveForm::begin(['method' => 'post'])?>
    <span style="width: 50%; float: left; display: block;"
        <?if (Yii::$app->promo->isActive()):?>
            data-toggle="tooltip" data-placement="top" title="<?=Yii::$app->promo->description?>"
        <?endif;?>
    >
                <input type="text" placeholder="Промокод" name="promo" style="width: 90%;padding: 8px; padding-left: 20px"
                    <?if (Yii::$app->promo->isActive()):?>
                        disabled value="<?=Yii::$app->promo->code?>"
                    <?endif;?>
                />
            </span>
    <?if (!Yii::$app->promo->isActive()):?>
        <input type="submit" value="Подтвердить" style="width: 45%;padding: 8px;">
    <?else:?>
        <input type="submit" value="Сбросить" name="resetPromo" style="width: 45%;padding: 8px;">
    <?endif;?>
    <?\yii\widgets\ActiveForm::end()?>
</div>

<h3>Корзина</h3>

<? \yii\widgets\Pjax::begin(['id' => 'pjax-carts'])?>
    <?foreach ($items as $item):?>
        <div class="mProduct">
            <div class="mProductName">
                <strong><?=$item->fullName?></strong>
            </div>
            <div class="mProductClose" data-id="<?=$item->id?>">&#10006;</div>
            <div class="mProduct2">
                <div class="mProductCount">
                    <div class="touchspin" data-id="<?=$item->id?>">
                        <div class="t_icon minus"><a href="#" onclick="return false;">-</a></div>
                        <div class="quantity"><?=number_format($item->count)?></div>
                        <div class="t_icon plus"><a href="#" onclick="return false;">+</a></div>
                    </div>
                </div>
                <div class="mProductPrice">
                    <?=$item->sum?> ₽
                </div>
            </div>
        </div>
    <?endforeach;?>

<button class="orderBtn" onclick="location.href='<?=\yii\helpers\Url::to(['/order/phone'])?>'">Продолжить</button>

<div class="cartFooter" style="">
    <div style="width: 100%; display: block">
        <div class="sum-title">
            Сумма заказа:
        </div>
        <div class="sum-text">
            <?=Yii::$app->cart->sumOrder?> ₽
        </div>
    </div>
    <?if (Yii::$app->cart->sumDiscount > 0):?>
        <div style="width: 100%; display: block">
            <div class="sum-title">
                Скидка:
            </div>
            <div class="sum-text">
                <?=Yii::$app->cart->sumDiscount?> ₽
            </div>
        </div>
        <div style="width: 100%;display: block">
            <div class="sum-title">
                Итого:
            </div>
            <div class="sum-text">
                <?=Yii::$app->cart->sumOrderWithDiscount?> ₽
            </div>
        </div>
    <?endif;?>
</div>

<? \yii\widgets\Pjax::end()?>
