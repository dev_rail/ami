<?php


namespace frontend\models\good;


class CategorySizes extends \common\models\good\CategorySizes
{
    public function getName()
    {
        return number_format($this->size, 0)." ".$this->unit;
    }
}