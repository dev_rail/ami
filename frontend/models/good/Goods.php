<?php


namespace frontend\models\good;


use yii\helpers\ArrayHelper;

class Goods extends \common\models\good\Goods
{
    public function getPhoto()
    {
        return $this->hasOne(GoodPhotos::class, ['good_id' => 'id']);
    }

    public function getSizes()
    {
        return $this->hasMany(GoodSizes::class, ['good_id' => 'id'])->orderBy('price ASC');
    }

    public function getPrice()
    {
        foreach ($this->sizes as $size)
            return $size->priceText;

        return number_format($this->price, 0);
    }

    public function getWeight(){
        foreach ($this->sizes as $size)
            return $size->weight;

        return false;
    }

    public function getFirstSizeId()
    {
        return ArrayHelper::getValue($this->getSizes()->one(), "id");
    }

    public function getCategoryGood()
    {
        return $this->hasOne(CategoryGood::class, ['good_id' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id'])->via('categoryGood');
    }

    public function getPhotoSrc()
    {
        return ArrayHelper::getValue($this, "photo.file.src");
    }
}
