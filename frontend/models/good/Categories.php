<?php


namespace frontend\models\good;


class Categories extends \common\models\good\Categories
{
    public function getGoods()
    {
        return $this->hasMany(Goods::class, ['id' => 'good_id'])->via('categoryGoods')->orderBy('sort');
    }

    public function getCategoryGoods()
    {
        return $this->hasMany(CategoryGood::class, ['category_id' => 'id']);
    }
}