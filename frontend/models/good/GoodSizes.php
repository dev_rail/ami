<?php


namespace frontend\models\good;


use yii\helpers\ArrayHelper;

class GoodSizes extends \common\models\good\GoodSizes
{
    public function getCategorySize()
    {
        return $this->hasOne(CategorySizes::class, ['id' => 'category_size_id']);
    }

    public function getName()
    {
        return ArrayHelper::getValue($this, "categorySize.name");
    }

    public function getPriceText(){
        return number_format($this->price, 0);
    }
}
