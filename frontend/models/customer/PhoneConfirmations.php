<?php


namespace frontend\models\customer;


use common\components\SmsHelper;

class PhoneConfirmations extends \common\models\customer\PhoneConfirmations
{
    public function sendSms()
    {
        $txt = "Код подтверждения: ".$this->code;
        $sms = new SmsHelper();

        $sms->send_sms($this->phone, $txt);
    }

    public function beforeSave($insert)
    {
        if ($insert)
            $this->sendSms();

        return parent::beforeSave($insert);
    }
}
