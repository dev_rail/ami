<?php

namespace frontend\models\customer;


class CustomerAddresses extends \common\models\customer\CustomerAddresses
{
    public function getStreet(){
        $result = $this->name;
        $array_result = explode(",", $result);
        if (!isset($array_result[1]))
            return "";

        $street = str_replace("ул.", "", $array_result[1]);

        return trim($street);
    }

    public function getHome(){
        $result = $this->name;
        $array_result = explode(",", $result);
        if (!isset($array_result[2]))
            return "";

        $place_text = "-".$this->place;
        $home = str_replace($place_text, "", $array_result[2]);

        return trim($home);
    }
}