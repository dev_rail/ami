<?php

namespace app\models;

use Yii;

class BackSessions extends \common\models\BackSessions{
    static function add(){
        $model = new self([
            'session_id' => Yii::$app->session->id,
            'customer_id' => Yii::$app->user->id
        ]);

        $model->save();
    }

    public function beforeDelete()
    {
        Yii::$app->user->identity->logout();
        return parent::beforeDelete();
    }
}