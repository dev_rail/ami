<?php


namespace frontend\models\order;


use common\models\order\OrderCustomer;
use common\models\order\OrderItems;
use common\models\order\OrderPromotion;
use common\models\order\OrderStatuses;
use frontend\models\Addresses;
use frontend\models\customer\Customers;
use yii\base\Model;

class AddressOrderModel extends Model
{
    public $address;
    public $home;
    public $place;

    public $description;
    public $type_pay;

    public $name;

    public function rules()
    {
        return [
            [['address', 'place', 'description', 'name', 'home'], 'string'],
            [['type_pay'], 'integer']
        ];
    }

    public function formOrder()
    {
        /** @var Customers $customer */
        $customer = \Yii::$app->user->identity;
        $cus_address = false;

        if ($this->address) {
            $address_text = "г. Казань, ул. " . $this->address.",".$this->home;
            $address = Addresses::getAddress($address_text);

            $cus_address = $customer->addAddress($address->id, $this->place);
        }

        $order = new Order(['created_at' => time(), 'type_pay' => $this->type_pay, 'description' => $this->description]);
        $order->save();

        $ord_customer = new OrderCustomer([
            'customer_id' => $customer->id
        ]);

        if ($cus_address) {
            $ord_customer->customer_address_id = $cus_address->id;
            $ord_customer->address_id = $address->id;
        }

        $order->link("orderCustomer", $ord_customer);

        $ord_status = new OrderStatuses(['date' => time(), 'status' => OrderStatuses::STATUS_NEW]);
        $order->link('orderStatuses', $ord_status);

        if (\Yii::$app->cart->sumDiscount > 0){
            $order_promotion = new OrderPromotion([
                'promotion_id' => \Yii::$app->promo->promo->id,
                'sum_discount' => \Yii::$app->cart->sumDiscount
            ]);

            $order->link('orderPromotion', $order_promotion);
        }

        /** @var Cart $item */
        foreach (\Yii::$app->cart->getAllItemsQuery()->all() as $item)
        {
            $ord_item = new OrderItems([
                'good_id' => $item->good_id,
                'good_size_id' => $item->good_size_id,
                'count' => $item->count,
                'price' => $item->price
            ]);

            $order->link('orderItems', $ord_item);
        }

        \Yii::$app->cart->reset();
        $order->sendSms();
        $order->sendTelegram();

        return $order;
    }
}
