<?php


namespace frontend\models\order;


use frontend\models\customer\Customers;

class OrderCustomer extends \common\models\order\OrderCustomer
{
    public function getCustomer()
    {
        return $this->hasOne(Customers::class, ['id' => 'customer_id']);
    }
}
