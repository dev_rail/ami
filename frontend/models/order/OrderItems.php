<?php


namespace frontend\models\order;


use frontend\models\good\Goods;
use frontend\models\good\GoodSizes;

class OrderItems extends \common\models\order\OrderItems
{
    public function getGood()
    {
        return $this->hasOne(Goods::class, ['id' => 'good_id']);
    }

    public function getGoodSize()
    {
        return $this->hasOne(GoodSizes::class, ['id' => 'good_size_id']);
    }
}
