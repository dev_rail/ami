<?php


namespace frontend\models\order;


class OrderLink extends \common\models\order\OrderLink
{
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }
}
