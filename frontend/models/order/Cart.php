<?php


namespace frontend\models\order;


use frontend\models\good\Goods;
use frontend\models\good\GoodSizes;
use yii\helpers\ArrayHelper;

class Cart extends \common\models\order\Cart
{
    public function getGood()
    {
        return $this->hasOne(Goods::class, ['id' => 'good_id']);
    }

    public function getGoodSize()
    {
        return $this->hasOne(GoodSizes::class, ['id' => 'good_size_id']);
    }

    public function getFullName()
    {
        $category = $this->good->category;
        return $category->name." ".$this->good->name." ".ArrayHelper::getValue($this, "goodSize.name");
    }
}
