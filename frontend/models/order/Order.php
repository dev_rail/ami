<?php


namespace frontend\models\order;


use common\models\order\OrderPay;
use common\models\order\Orders;
use common\models\settings\PaySettings;

class Order extends Orders
{
    public function getDateText()
    {
        $months = [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ];

        $dateText = $months[date("n", $this->created_at) - 1]." ".date("d", $this->created_at).", ".date("Y", $this->created_at);
        return $dateText." ".date("H:i", $this->created_at);
    }

    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::class, ['order_id' => 'id']);
    }

    public function addPay($pay_id, $url = null, $type = PaySettings::TYPE_YANDEX)
    {
        $order_pay = $this->orderPay;

        if (!$order_pay)
        {
            $order_pay = new OrderPay();

            $this->link('orderPay', $order_pay);
        }

        $order_pay->pay_id = $pay_id;
        $order_pay->type = $type;

        if ($url)
            $order_pay->url = $url;

        $order_pay->is_success = false;

        $order_pay->save();
    }

    public function getOrderCustomer()
    {
        return $this->hasOne(OrderCustomer::class, ['order_id' => 'id']);
    }
}
