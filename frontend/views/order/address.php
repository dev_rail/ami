<?php

/* @var $this \yii\web\View */
$this->title = "Детали доставки AMI пицца";
$js = <<<JS
    function changeAddress() {
        var input = $('#addresses');  
        var id = $(input).val();
        $.get('/ajax/address', {id: id}, function(data) {
            $("#street").val(data.street);
            $("#home").val(data.home);
            $("#place").val(data.place);
        });
    }
    
    $('#btnSubmit').on("click", function() {
      $(this).attr("disabled", true);
      $('#form').submit();
    });
JS;

$this->registerJs($js);
?>

<div class="pizzaro-order-steps">
    <ul>
        <li class="cart">
            <span class="step">1</span>Проверка
        </li>
        <li class="checkout">
            <span class="step">2</span>Оформление
        </li>
        <li class="complete">
            <span class="step">3</span>Заказ завершен
        </li>
    </ul>
</div>

<div id="post-9" class="post-9 page type-page status-publish hentry">
    <div class="entry-content">
        <div class="woocommerce">
            <?$form = \yii\widgets\ActiveForm::begin(['id' => 'form', 'options' => ['class' => 'checkout woocommerce-checkout']])?>
                <div class="col2-set" id="customer_details">
                    <div class="col-1">
                        <?=\yii\bootstrap\Alert::widget([
                            'options' => [
                                'class' => 'alert-info',
                            ],
                            'body' => 'Поля можно оставить пустыми. В данном случае наш менеджер свяжется с Вами для уточнения необходимой информации.'
                        ])?>
                        <div class="woocommerce-billing-fields">
                            <p class="form-row form-row form-row-first validate-required validate-phone" id="billing_phone_field">
                                <label for="billing_phone" class="">Телефон </label>
                                <input type="tel" class="input-text " name="billing_phone" id="billing_phone"  autocomplete="tel" value="<?=Yii::$app->user->identity->phone?>" disabled/>
                            </p>
                            <div class="clear"></div>
                            <h3>Адрес доставки</h3>
                            <?if (Yii::$app->user->identity->addresses):?>
                                <p class="form-row form-row" id="billing_address_1_field">
                                    <label for="billing_address_1" class="">Ваши сохраненные адреса</label>
                                    <?=\kartik\select2\Select2::widget([
                                        'data' => Yii::$app->user->identity->addresses,
                                        'name' => 'addresses',
                                        'pluginEvents' => [
                                            "change" => "function(data) { changeAddress(); }"
                                        ],
                                        'options' => [
                                            'placeholder' => 'Выберите адрес',
                                            'id' => 'addresses'
                                        ]
                                    ])?>
                                </p>
                            <?endif;?>
                            <div class="clear"></div>
                            <p class="form-row form-row address-field validate-required" id="billing_address_1_field">
                                <label for="billing_address_1" class="">Улица</label>
                                <input type="text" class="input-text " name="AddressOrderModel[address]"
                                       id="street" placeholder="Улица"  autocomplete="address-line1" value=""
                                    <?=Yii::$app->user->identity->backSession?"required":""?>/>
                            </p>
                            <p class="form-row form-row form-row-first address-field validate-required" id="billing_address_1_field">
                                <label for="billing_address_1" class="">Дом</label>
                                <input type="text" class="input-text " name="AddressOrderModel[home]"
                                       id="home" placeholder="Дом"  autocomplete="address-line1" value=""
                                    <?=Yii::$app->user->identity->backSession?"required":""?>/>
                            </p>
                            <p class="form-row form-row form-row-last address-field validate-required" id="billing_address_1_field">
                                <label for="billing_address_1" class="">Квартира/Офис</label>
                                <input type="text" class="input-text " name="AddressOrderModel[place]" id="place" placeholder="Квартира или офис"  autocomplete="address-line1" value=""  />
                            </p>

                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="woocommerce-shipping-fields">
                            <p class="form-row form-row notes" id="order_comments_field">
                                <label for="order_comments" class="">Комментарий к заказу</label>
                                <textarea name="AddressOrderModel[description]" class="input-text " id="order_comments" placeholder="Напишите комментарий к заказу. Например, не звонить в домофон"    rows="2" cols="5"></textarea>
                            </p>
                        </div>
                    </div>
                </div>
                <h3 id="order_review_heading">Ваш заказ</h3>
                <div id="order_review" class="woocommerce-checkout-review-order">
                    <table class="shop_table woocommerce-checkout-review-order-table">
                        <thead>
                        <tr>
                            <th class="product-name">Продукт</th>
                            <th class="product-total">Всего</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? /** @var \frontend\models\order\Cart $item */
                        foreach ($items as $item):?>
                            <tr class="cart_item">
                                <td class="product-name">
                                    <?=$item->fullName?>&nbsp;<strong class="product-quantity">&times; <?=$item->count?></strong>
                                    <dl class="variation">
                                    </dl>
                                </td>
                                <td class="product-total">
                                                    <span class="woocommerce-Price-amount amount">
                                                    <span class="woocommerce-Price-currencySymbol">₽</span><?=$item->sum?></span>
                                </td>
                            </tr>
                        <?endforeach;?>
                        </tbody>
                        <tfoot>
                        <tr class="order-total">
                            <th>Всего</th>
                            <td>
                                <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Р</span><?=Yii::$app->cart->sumOrder?></span></strong>
                            </td>
                        </tr>
                        <?if (Yii::$app->cart->sumDiscount > 0):?>
                            <tr>
                                <th>Скидка</th>
                                <td>
                                    <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Р</span><?=Yii::$app->cart->sumDiscount?></span></strong>
                                </td>
                            </tr>
                            <tr>
                                <th>Итого</th>
                                <td>
                                    <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Р</span><?=Yii::$app->cart->sumOrderWithDiscount?></span></strong>
                                </td>
                            </tr>
                        <?endif?>
                        </tfoot>
                    </table>
                    <div id="payment" class="woocommerce-checkout-payment">
                        <ul class="wc_payment_methods payment_methods methods">
                            <li class="wc_payment_method payment_method_cod">
                                <input id="payment_method_cod" type="radio" class="input-radio" name="AddressOrderModel[type_pay]" value=<?=\frontend\models\order\Order::TYPE_PAY_CASH?>  data-order_button_text="" checked/>
                                <label for="payment_method_cod">Наличными при получении   </label>
                                <div class="payment_box payment_method_cod" style="display:none;">
                                    <!-- <p>Pay with cash upon delivery.</p> -->
                                </div>
                            </li>
                            <li class="wc_payment_method payment_method_paypal">
                                <input id="payment_method_paypal" type="radio" class="input-radio" name="AddressOrderModel[type_pay]" value=<?=\frontend\models\order\Order::TYPE_PAY_ELECTRON?>  data-order_button_text="Proceed to PayPal" />
                                <label for="payment_method_paypal">Оплата картой онлайн <img alt="PayPal Acceptance Mark" src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg"/>
                                    <!-- <a title="What is PayPal?" onclick="javascript:window.open('https://www.paypal.com/us/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" class="about_paypal" href="https://www.paypal.com/us/webapps/mpp/paypal-popup"   ></a>  </label> -->
                                    <div class="payment_box payment_method_paypal" style="display:none;">
                                        <!--  <p>Pay via PayPal; you can pay with your credit card if you don&#8217;t have a PayPal account.</p> -->
                                    </div>
                            </li>
                        </ul>
                        <div class="form-row place-order">
                            <noscript>Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.<br/>
                                <input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" />
                            </noscript>
                            <button class="button alt" style="text-align: center;" type="submit" id="btnSubmit">Завершить заказ</button>
                        </div>
                    </div>
                </div>
            <? \yii\widgets\ActiveForm::end()?>
        </div>
    </div>
    <!-- .entry-content -->
</div>


