<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\models\order\Order */
$this->title = "Детали доставки AMI пицца";
?>

<div id="post-9" class="post-9 page type-page status-publish hentry">
    <div class="entry-content">
        <div class="woocommerce">
            <?if ($model->status != \common\models\order\OrderStatuses::STATUS_COMPLETE && $model->status != \common\models\order\OrderStatuses::STATUS_CANCEL):?>
                <?=$this->render('camera')?>
            <?endif;?>
            <table class="shop_table customer_details">
                <tbody>
                <tr>
                    <th>Статус:</th>
                    <td><?=$model->statusText?></td>
                </tr>
                </tbody>
            </table>
            <ul class="woocommerce-thankyou-order-details order_details">
                <li class="order">Номер заказа:<strong><?=$model->id?></strong></li>
                <li class="date">Дата и время:<strong><?=$model->dateText?></strong></li>
                <li class="total">Итого:<strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₽</span><?=$model->sum?></span></strong></li>
                <li class="method">Тип оплаты:<strong><?=$model->typePayText?></strong></li>
            </ul>

            <table class="shop_table customer_details">
                <?foreach ($model->orderItems as $item):?>
                    <tr class="order_item">
                        <td class="product-name">
                            <a href="#"><?=$item->goodFullName?></a> <strong class="product-quantity">× <?=$item->count?></strong>
                        </td>
                        <td class="product-total"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₽</span><?=$item->sum?></span>  </td>
                    </tr>
                <?endforeach;?>
            </table>

            <?if ($model->type_pay == \frontend\models\order\Order::TYPE_PAY_ELECTRON):?>
                <table class="shop_table customer_details">
                    <tbody>
                    <tr>
                        <td style="text-align: center">
                            <?if ($model->paidOnline):?>
                                <strong>Оплачен</strong>
                            <?else:?>
                                <?=\yii\helpers\Html::a('Оплатить', \yii\helpers\Url::to(['/order/pay', 'id' => $model->id]), ['class' => 'btn btn-success'])?>
                            <?endif;?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            <?endif;?>
            <div class="clear"></div>
            <!--<p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>-->
            <header>
                <h2>Информация о покупателе</h2>
            </header>
            <table class="shop_table customer_details">
                <tbody>
                <tr>
                    <th>Телефон:</th>
                    <td><?=Yii::$app->user->identity->phone?></td>
                </tr>
                </tbody>
            </table>
            <?if ($model->orderCustomer->customerAddress):?>
                <header class="title">
                    <h3>Адрес</h3>
                </header>
                <address><?=$model->orderCustomer->customerAddress->getFullAddressText()?></address>
            <?endif;?>
        </div>
    </div>
    <!-- .entry-content -->
</div>
<!-- #post-## -->
