<?php

/* @var $this \yii\web\View */

$this->title = "Оценка качества обслуживания";
?>

<div id="post-9" class="post-9 page type-page status-publish hentry">
    <header class="entry-header">
        <h1 class="entry-title"><?=$this->title?></h1>
    </header>

    <hr>
    <?$form = \yii\widgets\ActiveForm::begin()?>
        <?=$form->field($model, "raiting_product")->widget(\kartik\rating\StarRating::class, [
            'pluginOptions' => [
                'size' => 'xl'
            ],
        ])?>
        <?=$form->field($model, "raiting_delivery")->widget(\kartik\rating\StarRating::class, [
            'pluginOptions' => [
                'size' => 'xl'
            ],
        ])?>
        <?=$form->field($model, "description")->textarea()?>
        <?=\yii\helpers\Html::submitButton('Отправить', ['style' => 'margin: 0 auto; display: block;'])?>
    <? \yii\widgets\ActiveForm::end()?>
</div>
