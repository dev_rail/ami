<?php


/* @var $this \yii\web\View */
$this->title = "Спасибо!";
?>

<div id="post-9" class="post-9 page type-page status-publish hentry">
    <header class="entry-header">
        <h1 class="entry-title"><?=$this->title?></h1>
    </header>
    <hr>

    <div>
        Благодарим за уделенное Вами время! Мы будем стараться стать лучше!
    </div>
</div>
