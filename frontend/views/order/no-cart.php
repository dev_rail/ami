<?php



/* @var $this \yii\web\View */
$this->title = "Корзина Amipizza";
?>

<div class="error-404 not-found">
    <div class="page-content">
        <header class="page-header">
            <h1 class="page-title">Корзина пуста!</h1>
        </header>
        <!-- .page-header -->
        <p>Ничего не найдено. Попробуйте добавить товары.</p>

    </div>
    <!-- .page-content -->
</div>
