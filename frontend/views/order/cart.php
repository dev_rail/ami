<?php

/* @var $this \yii\web\View */
/* @var $items \frontend\models\order\Cart[] */


$script = <<<JS
    $(document).ready(function () {
        var sendcode = false;
        
        $("#modal-phone").iziModal({
            title: 'Авторизация',
            headerColor: '#bd6363',
        });
        
        $(document).ready(function() {
          $('[data-toggle="tooltip"]').tooltip();
        });
        
        $(".phone-input").mask("+7 999 999-99-99");
        
        $(document).on("click", ".rm-cart", function() {
            cart_id = $(this).attr("data-id");
            $.post("/cart/delete", {id:cart_id}, function() {
                $.pjax.reload({container:"#pjax-carts"});
                $.post("/cart/get-text", function(data) {
                    $("#cartText").html(data.message);
                });
            });
        });
        
        $(document).on("click", ".t_icon", function() {
            url = "/cart/";
            
            if ($(this).hasClass("minus"))
                url = url + "minus";
            else if ($(this).hasClass("plus"))
                url = url + "plus";
            
            cart_id = $(this).parent(".touchspin").attr("data-id");
            
            $.post(url, {cart_id:cart_id}, function() {
                $.pjax.reload({container:"#pjax-carts"});
                $.post("/cart/get-text", function(data) {
                    $("#cartText").html(data.message);
                });
            });
        });
        
        $(document).on('keyup', ".phone-input", function() {
            regexp = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
            if ($(this).val().search(regexp) == 0)
                $(".phone-button").attr("disabled", false);
            else 
                $(".phone-button").attr("disabled", true);
        });
        
        $(document).on('keyup', ".phone-input", function(e) {
            if (e.which == 13)
                $('.phone-button').click();
        });
        
        $(document).on('keyup', ".code-input", function(e) {
            if (e.which == 13)
                $('.code-button').click();
        });
        
        $(document).on("click", ".phone-button", function() {
            regexp = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
            phone = $(".phone-input").val();
            
            if (phone.search(regexp) == -1)
                return false;
            
            $(".phone-text").html(phone);
            $("#modal-phone").iziModal('setContent', $("#modal-code").html());
            $(".code-input").mask("9999");
            
            url = "/phone-confirmation/get-code";
            $.post(url, {phone: phone});
        });
        
        $(document).on('keyup', ".code-input", function() {
            code = $(this).val();
            regexp = /\d{4}/;
            
            if (code.search(regexp) == 0)
                $(".code-button").attr("disabled", false);
            else 
                $(".code-button").attr("disabled", true);
        });
        
        $(document).on('click', ".code-button", function() {
            if (sendcode == true){
                sendcode = false;
                return false;
            }
            
            sendcode = true;
            
            code = $("#modal-phone .code-input").val();
            regexp = /\d{4}/;
            
            if (code.search(regexp) == -1)
                return false;
            
            url = "/phone-confirmation/confirm";
            $.post(url, {code: code}, function(data) {
                console.log(data);
                $('.code-error').html(data.message);
            });
        });
    });
JS;

$this->registerJs($script);

$this->title = "Корзина Amipizza";
?>

<style>

    .touchspin{
        overflow: hidden;
        font-size: 28px;
    }

    .touchspin div{
        float: left;
        text-align: center;
    }

    .t_icon{
        background: #c03232;
        display: block;
        width: 20%;
        border-radius: 50%;
    }

    .t_icon a{
        color: white;
        font-weight: bold;
        display: block;
    }

    .quantity{
        width: 60%;
    }

    .modal-phone-block{
        padding: 20px;
    }

    .modal-code-block{
        padding: 20px;
    }

    .phone-button{
        float: right;
    }

    .phone-text{
        color: black;
    }

    .tooltip > .tooltip-inner {
        background: #b1b1b1;
        color: #FFFFFF;
        padding: 15px;
        font-size: 14px;
        font-weight: bold;
    }
</style>

<div class="pizzaro-order-steps">
    <ul>
        <li class="cart">
            <span class="step">1</span>Проверка заказа
        </li>
        <li class="checkout">
            <span class="step">2</span>Оформление заказа
        </li>
        <li class="complete">
            <span class="step">3</span>Заказ завершен
        </li>
    </ul>
</div>

<div style="margin: 0 auto; width: 50%;">
    <div class="promo">
        <?if (Yii::$app->session->hasFlash('error-promo')):?>
            <?=\yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'alert-danger',
                ],
                'body' => Yii::$app->session->getFlash('error-promo')
            ])?>
        <?elseif (Yii::$app->session->hasFlash('success-promo')):?>
            <?=\yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'alert-success',
                ],
                'body' => Yii::$app->session->getFlash('success-promo')
            ])?>
        <?endif;?>
        <?$form = \yii\widgets\ActiveForm::begin(['method' => 'post'])?>
        <span style="width: 50%; float: left; display: block; margin-right: 20px;"
            <?if (Yii::$app->promo->isActive()):?>
                data-toggle="tooltip" data-placement="top" title="<?=Yii::$app->promo->description?>"
            <?endif;?>
        >
                    <input type="text" placeholder="Промокод" name="promo" style="width: 100%;padding: 8px; padding-left: 20px"
                        <?if (Yii::$app->promo->isActive()):?>
                            disabled value="<?=Yii::$app->promo->code?>"
                        <?endif;?>
                    />
                </span>
        <?if (!Yii::$app->promo->isActive()):?>
            <input type="submit" value="Подтвердить" style="width: 45%;padding: 8px;">
        <?else:?>
            <input type="submit" value="Сбросить" name="resetPromo" style="width: 45%;padding: 8px;">
        <?endif;?>
        <?\yii\widgets\ActiveForm::end()?>
    </div>
</div>

<? \yii\widgets\Pjax::begin(['id' => 'pjax-carts'])?>
    <div id="post-8" class="post-8 page type-page status-publish hentry">
    <div class="entry-content">
        <div class="woocommerce">
                <table class="shop_table shop_table_responsive cart" >
                    <thead>
                    <tr>
                        <th class="product-remove">&nbsp;</th>
                        <th class="product-thumbnail">&nbsp;</th>
                        <th class="product-name">Продукт</th>
                        <th class="product-price">Цена</th>
                        <th class="product-quantity">Количество</th>
                        <th class="product-subtotal">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach ($items as $item):?>
                        <tr class="cart_item">
                            <td class="product-remove">
                                <a href="#" class="remove rm-cart" onclick="return false;" data-id="<?=$item->id?>">&times;</a>
                            </td>
                            <td class="product-thumbnail">
                                <a href="#">
                                    <img width="180" height="180" src="<?=$item->good->photoSrc?>" alt=""/>
                                </a>
                            </td>
                            <td class="product-name" data-title="Продукт">
                                <a href="#" onclick="return false;"><?=$item->fullName?></a>
                            </td>
                            <td class="product-price" data-title="Цена">
                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₽</span><?=$item->price?></span>
                            </td>
                            <td class="product-quantity" data-title="Количество">
                                <div class="qty-btn">
                                    <div class="touchspin" data-id="<?=$item->id?>">
                                        <div class="t_icon minus"><a href="#" onclick="return false;">-</a></div>
                                        <div class="quantity"><?=number_format($item->count)?></div>
                                        <div class="t_icon plus"><a href="#" onclick="return false;">+</a></div>
                                    </div>
                                </div>
                            </td>
                            <td class="product-subtotal" data-title="Итого">
                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₽</span><?=$item->sum?></span>
                            </td>
                        </tr>
                    <?endforeach;?>
                    <tr class="cart_iteml">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Итого: </td>
                        <td data-title="Итого">
                            <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₽</span><?=Yii::$app->cart->sumOrder?></span></strong>
                        </td>
                    </tr>
                    <?if (Yii::$app->promo->isDiscountSum()):?>
                        <tr class="cart_iteml">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Скидка: </td>
                            <td data-title="Скидка">
                                <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₽</span><?=Yii::$app->cart->sumDiscount?></span></strong>
                            </td>
                        </tr>
                        <tr class="cart_iteml">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="2" style="text-align: center">Итого с учетом скидки: </td>
                            <td data-title="Итого с учетом скидки">
                                <strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₽</span><?=Yii::$app->cart->sumOrderWithDiscount?></span></strong>
                            </td>
                        </tr>
                    <?endif;?>
                    <tr>
                        <td colspan="6" class="actions">
                            <div class="wc-proceed-to-checkout">
                                <?if (Yii::$app->user->isGuest):?>
                                    <a href="#" class="checkout-button button alt wc-forward" data-izimodal-open="#modal-phone" data-izimodal-transitionin="fadeInDown">Продолжить оформление</a>
                                <?else:?>
                                    <a href="<?=\yii\helpers\Url::to(['/order/address'])?>" class="checkout-button button alt wc-forward" data-pjax="0">Продолжить оформление</a>
                                <?endif;?>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
        </div>
    </div>
</div>
<? \yii\widgets\Pjax::end()?>

<div id="modal-phone" style="display:none;">
    <div class="modal-phone-block">
        <h4>Номер телефона</h4>
        <input type="text" class="phone-input">
        <button  class="phone-button" type="button" disabled>Выслать код</button>
    </div>
</div>


<div id="modal-code" style="display:none;">
    <div class="modal-code-block">
        <b>Телефон: <span class="phone-text"></span></b>
        <h4>Код</h4>
        <input type="text" class="code-input">
        <button type="button" class="code-button" disabled>Подтвердить</button>
        <div style="color:red;" class="code-error"></div>
    </div>
</div>


