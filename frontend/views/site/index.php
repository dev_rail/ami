<?php

/* @var $this yii\web\View */

use yii\web\View;

/* @var $this yii\web\View */
/* @var $categories \frontend\models\good\Categories[] */

$this->title = 'Бесплатная доставка пиццы по Казани, халяль';
?>
<style>
    .ywapo_input_container_radio .size-active::before{
        background: #b1b1b1;
    }

    .product{
        height: 660px;
    }

    .promo{
        width: 90%;
        text-align: center;
        display: block;
        margin: 0 auto;
    }

    .price-btn{
        padding: 15px;
        background: #c00a27;
        box-sizing: border-box;
        border-radius: 35px;
        color: white;
        box-shadow:
                inset 0 2px 2px #e24b4b,
                inset 1px 0 3px #e24b4b,
                inset -1px 0 3px #e24b4b,
                inset 0 -2px 2px #e24b4b,
                0 0 3px rgba(0, 0, 0, 0.24);
    }

    .tooltip > .tooltip-inner {
        background: #b1b1b1;
        color: #FFFFFF;
        padding: 15px;
        font-size: 14px;
        font-weight: bold;
    }
</style>



<?
    $script = <<<JS
    $(document).ready(function () {
        $(".ywapo_size").click(function () {
            price = $(this).attr("data-price");
            good_id = $(this).attr("data-good-id");
            good_size_id = $(this).attr("data-good-size-id");
            weight = $(this).attr("data-good-weight");
            $(this).parent(".ywapo_group_container").children(".ywapo_input_container").children("span").removeClass('size-active');
            $(".ywapo_price[data-good-id = '" + good_id + "'] b .price-value").html(price);
            $(this).children(".ywapo_label_tag_position_after").addClass("size-active");
            $(".product_type_simple[data-good-id = '"+good_id+"']").attr("data-good-size-id", good_size_id);
            /*
            if (weight){
                $(".product-weight[data-good-id = '"+good_id+"']").css("display", "block");
                $(".product-weight[data-good-id = '"+good_id+"'] span").html(weight);
            }
            else 
                $(".product-weight[data-good-id = '"+good_id+"']").css("display", "none");*/
        });
        
        
        $('[data-toggle="tooltip"]').tooltip();
        
        $(".product_type_simple").click(function() {
            good_id = $(this).attr("data-good-id");
            good_size_id = $(this).attr("data-good-size-id");
            
            var jqxhr = $.post("/cart/add", {
                good_id: good_id,
                good_size_id: good_size_id
            }, function(data) {
                $.notify(data.message, "success");
                $.post("/cart/get-text", function(data) {
                    $("#cartText").html(data.message);
                    $("#mCartCount").html(data.mobMessage);
                });
            });
            
        });
        
    });
JS;

    $this->registerJs($script);

?>

<div class="section-products">
<div class="promo">
        <?if (Yii::$app->session->hasFlash('error-promo')):?>
            <?=\yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'alert-danger',
                ],
                'body' => Yii::$app->session->getFlash('error-promo')
            ])?>
        <?elseif (Yii::$app->session->hasFlash('success-promo')):?>
            <?=\yii\bootstrap\Alert::widget([
                'options' => [
                    'class' => 'alert-success',
                ],
                'body' => Yii::$app->session->getFlash('success-promo')
            ])?>
        <?endif;?>
        <?$form = \yii\widgets\ActiveForm::begin(['method' => 'post'])?>
            <span
                <?if (Yii::$app->promo->isActive()):?>
                    data-toggle="tooltip" data-placement="top" title="<?=Yii::$app->promo->description?>"
                <?endif;?>
            >
                <input type="text" placeholder="Введите промокод" style="margin: 0 20px 20px 20px;" name="promo"
                    <?if (Yii::$app->promo->isActive()):?>
                        disabled value="<?=Yii::$app->promo->code?>"
                    <?endif;?>
                />
            </span>
        <?if (!Yii::$app->promo->isActive()):?>
            <input type="submit" value="Подтвердить">
        <?else:?>
            <input type="submit" value="Сбросить" name="resetPromo">
        <?endif;?>
        <?\yii\widgets\ActiveForm::end()?>
    </div>
    <div class="woocommerce columns-3">
        <div class="columns-3">
            <ul class="products">
                <?foreach ($categories as $category):?>
                    <? /** @var \frontend\models\good\Goods $good */
                    foreach ($category->goods as $good):?>
                        <li class="product">
                            <div class="product-outer">
                                <div class="product-inner">
                                    <div class="product-image-wrapper">
                                        <a href="#" class="woocommerce-LoopProduct-link" onclick="return false;">
                                            <img src="<?=$good->photoSrc?>" class="img-responsive" alt="">
                                        </a>
                                        <?/*if ($good->weight):?>
                                            <div class="product-weight" data-good-id="<?=$good->id?>"><span><?=$good->weight?></span> гр.</div>
                                        <?endif;*/?>
                                    </div>
                                    <div class="product-content-wrapper">
                                        <a href="#" class="woocommerce-LoopProduct-link" onclick="return false;">
                                            <span style="margin-bottom: 25px; display: block; font-size: 17px;"><b><?=$good->name?></b></span>
                                            <div itemprop="description" style="display: block;">
                                                <p style="max-height: none;"><?=$good->description?></p>
                                            </div>
                                            <div class="ywapo_price" data-good-id="<?=$good->id?>" style="margin-bottom: 20px;"><b class="price-btn"><span class="price-value"><?=$good->getPrice()?></span> ₽</b></div>
                                            <?if ($good->getSizes()->count() > 0):?>
                                                <div  class="yith_wapo_groups_container">
                                                    <div  class="ywapo_group_container ywapo_group_container_radio form-row form-row-wide " data-requested="1" data-type="radio" data-id="1" data-condition="">
                                                        <h3><span>Выбери размер</span></h3>
                                                        <?$a = "size-active";?>
                                                        <? /** @var \frontend\models\good\GoodSizes $size */
                                                        foreach ($good->sizes as $size):?>
                                                            <div class="ywapo_input_container ywapo_input_container_radio ywapo_size" data-price="<?=$size->priceText?>" data-good-id="<?=$good->id?>" data-good-size-id="<?=$size->id?>" data-good-weight="<?=$size->weight?>">
                                                                <span class="ywapo_label_tag_position_after <?=$a?>"><span class="ywapo_option_label ywapo_label_position_after"><?=$size->categorySize->name?></span></span>
                                                            </div>
                                                            <?$a = "";?>
                                                        <?endforeach;?>
                                                    </div>
                                                </div>
                                            <?endif;?>
                                        </a>
                                        <div class="hover-area">
                                            <a rel="nofollow" onclick="return false;" href="#" data-good-id="<?=$good->id?>" data-good-size-id="<?=$good->firstSizeId?>" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Добавить в корзину</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.product-outer -->
                        </li>
                    <?endforeach;?>
                <?endforeach;?>
                <!-- /.products -->
            </ul>
        </div>
    </div>
</div>
