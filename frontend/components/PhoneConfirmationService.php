<?php


namespace frontend\components;

use frontend\models\customer\Customers;
use frontend\models\customer\PhoneConfirmations;
use yii\base\Component;

class PhoneConfirmationService extends Component
{
    private $session_id;

    public function init()
    {
        \Yii::$app->session->open();
        $this->session_id = \Yii::$app->session->id;
    }

    public function getCodeByPhone($phone)
    {
        $phone = str_replace(" ", "", $phone);
        $phone = str_replace("-", "", $phone);

        PhoneConfirmations::deleteAll(['phone' => $phone]);
        PhoneConfirmations::deleteAll(['session_id' => $this->session_id]);

        $model = new PhoneConfirmations([
            'session_id' => $this->session_id,
            'phone' => $phone,
            'code' => mt_rand(1000, 9999)
        ]);

        $model->save();
    }

    public function confirmByCode($code)
    {
        $model = PhoneConfirmations::findOne(['session_id' => $this->session_id]);

        if ($model->code == $code)
        {
            $customer = Customers::findOne(['phone' => $model->phone]);

            if (!$customer)
            {
                $customer = new Customers([
                    'phone' => $model->phone
                ]);

                $customer->save();
            }

            $customer->auth();

            $model->delete();

            return true;
        }
        else
        {
            $model->attempts += 1;
            $model->save();

            return $model;
        }
    }
}
