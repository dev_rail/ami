<?php

namespace frontend\components;


use common\components\SberbankPay;
use common\components\YandexPay;
use common\models\settings\PaySettings;
use frontend\models\order\Order;
use yii\base\Component;

class OnlinePay extends Component
{
    public function pay(Order $order){
        $pay_setting = $this->getModel();

        switch ($pay_setting->type){
            case PaySettings::TYPE_YANDEX:
                $pay = new YandexPay();
                return $pay->pay($order);
            case PaySettings::TYPE_SBERBANK:
                $pay = new SberbankPay();
                return $pay->pay($order);
        }
    }

    private function getModel(){
        $model = PaySettings::find()->one();

        if (!$model){
            $model = new PaySettings(['type' => PaySettings::TYPE_YANDEX]);
            $model->save();
        }

        return $model;
    }
}