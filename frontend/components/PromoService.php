<?php

namespace frontend\components;


use frontend\models\promotions\Promotions;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class PromoService extends Component
{
    private $promo_id = false;
    public $promo = false;

    public function init()
    {
        $this->promo_id = \Yii::$app->session->get('promo_id', false);

        if ($this->promo_id){
            $promo = Promotions::findOne($this->promo_id);
            if (!$promo) {
                $this->promo_id = false;
                \Yii::$app->session->remove('promo_id');
            }
            else
                $this->promo = $promo;
        }
        parent::init();
    }

    public function isActive(){
        if ($this->promo_id)
            return true;

        return false;
    }

    public function getCode(){
        return ArrayHelper::getValue($this->promo, "code");
    }

    public function getDescription(){
        return ArrayHelper::getValue($this->promo, "description");
    }

    public function reset(){
        \Yii::$app->session->remove('promo_id');
        $this->promo_id = false;
        $this->promo = false;
    }

    public function isDiscountSum(){
        if ($this->promo && $this->promo->type == Promotions::TYPE_DISCOUNT)
            return true;

        return false;
    }

    public function getDiscountValue(){
        if (!$this->promo)
            return false;

        switch ($this->promo->type){
            case Promotions::TYPE_DISCOUNT:
                return ArrayHelper::getValue($this->promo, "discountValue.value");
        }


        return false;
    }

    public function findPromo(string $promo)
    {
        $model = Promotions::findOne(['code' => $promo]);

        if (!$model)
            Yii::$app->session->setFlash('error-promo', 'Код не найден!');
        else{
            Yii::$app->session->setFlash('success-promo', $model->description);
            Yii::$app->session->set('promo_id', $model->id);
            $this->promo_id = $model->id;
            $this->promo = $model;
        }
    }
}