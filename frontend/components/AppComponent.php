<?php


namespace frontend\components;


use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\base\Theme;

class AppComponent extends Component implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        \Yii::$app->on(Application::EVENT_BEFORE_REQUEST, function() {
            if (\Yii::$app->devicedetect->isMobile()) {
                \Yii::$app->view->theme = new Theme([
                    'pathMap' => [
                        '@frontend/views' => [
                            '@frontend/templates/mobile',
                        ],
                    ]
                ]);
            }
        });
    }
}
