<?php


namespace frontend\components;


use frontend\models\order\Cart;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CartService extends Component
{
    private $session_id;

    public function init()
    {
        \Yii::$app->session->open();
        $this->session_id = \Yii::$app->session->id;
    }

    public function getSumOrder()
    {
        $sum = $this->getAllItemsQuery()->sum('sum');
        return $sum;
    }

    public function getSumDiscount(){
        $sum = $this->sumOrder;
        $discount = \Yii::$app->promo->discountValue;
        $sum_discount = number_format(($sum * $discount)/100, 2);

        return $sum_discount;
    }

    public function getSumOrderWithDiscount(){
        return number_format($this->getSumOrder() - $this->getSumDiscount(), 2);
    }

    public function getTextCart()
    {
        $count = $this->getAllItemsQuery()->count('*');

        if ($count == 0)
            return Html::tag('span', 'Остутствуют', ['class' => 'count']);


        return Html::tag('span', $count." поз.", ['class' => 'count']).Html::tag('span', "₽".$this->getSumOrder(), ['class' => 'amount']);
    }

    public function getMobileTextCart()
    {
        $count = $this->getAllItemsQuery()->count('*');

        if ($count == 0)
            return "";


        return Html::tag('span', $count, ['class' => 'count']);
    }


    public function add($good_id, $good_size_id = false)
    {
        $query = Cart::find()->where(['good_id' => $good_id, 'session_id' => $this->session_id]);

        if ($good_size_id)
            $query->andWhere(['good_size_id' => $good_size_id]);

        $model = $query->one();

        if (!$model)
        {
            $model = new Cart([
                'session_id' => $this->session_id,
                'good_id' => $good_id,
                'good_size_id' => $good_size_id,
                'count' => 0
            ]);
        }

        $model->count += 1;
        $model->save();

        $text = "Добавлено: ";

        $category = $model->good->category;
        $text .= $category->name." ".$model->good->name." ".ArrayHelper::getValue($model, "goodSize.name");

        return $text;
    }

    public function delete($cart_id)
    {
        $cart = Cart::findOne(["id" => $cart_id, 'session_id' => $this->session_id]);

        if ($cart)
            $cart->delete();
    }

    public function plusOne($cart_id)
    {
        $cart = Cart::findOne(["id" => $cart_id, 'session_id' => $this->session_id]);

        if ($cart)
        {
            $cart->count += 1;
            $cart->save();
        }
    }

    public function minusOne($cart_id)
    {
        $cart = Cart::findOne(["id" => $cart_id, 'session_id' => $this->session_id]);

        if ($cart)
        {
            if ($cart->count == 1)
            {
                $cart->delete();
            }
            else {
                $cart->count -= 1;
                $cart->save();
            }
        }
    }

    public function getAllItemsQuery()
    {
        return Cart::find()->where(['session_id' => $this->session_id]);
    }

    public function updateSession($session_id)
    {
        $items = Cart::find()->where(['session_id' => $session_id])->all();

        /** @var Cart $item */
        foreach ($items as $item)
        {
            $item->session_id = $this->session_id;
            $item->save();
        }
    }

    public function reset()
    {
        Cart::deleteAll(['session_id' => $this->session_id]);
    }
}
