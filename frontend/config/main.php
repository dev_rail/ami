<?php

use frontend\components\AppComponent;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'devicedetect', 'appComponent'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'devicedetect' => [
            'class' => \alexandernst\devicedetect\DeviceDetect::class
        ],
        'appComponent' => [
            'class' => AppComponent::className()
        ],
        'cart' => [
            'class' => \frontend\components\CartService::class
        ],
        'phoneConfirmation' => [
            'class' => \frontend\components\PhoneConfirmationService::class
        ],
        'promo' => [
            'class' => \frontend\components\PromoService::class
        ],
        'user' => [
            'identityClass' => \frontend\models\customer\Customers::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => ['site/index'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'l/<id>' => 'l/index',
                'l/r/<id>' => 'l/r',
                'l/get-order/<id>' => 'l/get-order'
            ],
        ],
    ],
    'params' => $params,
];
