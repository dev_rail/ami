<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/assets/css/bootstrap.min.css',
        '/assets/css/font-awesome.min.css',
        '/assets/css/animate.min.css',
        '/assets/css/font-pizzaro.css',
        '/assets/css/style.css',
        '/assets/css/colors/red.css',
        '/assets/css/jquery.mCustomScrollbar.min.css',
        //'css/site.css',
        'https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.min.css'
    ];
    public $js = [
        //'assets/js/jquery.min.js',
        '/assets/js/tether.min.js',
        '/assets/js/bootstrap.min.js',
        '/assets/js/owl.carousel.min.js',
        '/assets/js/social.share.min.js',
        '/assets/js/jquery.mCustomScrollbar.concat.min.js',
        '/assets/js/scripts.min.js',
        '/assets/js/notify.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js',
        '/js/jquery.maskedinput.min.js',
        //'https://unpkg.com/popper.js',
        //'https://unpkg.com/tippy.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
