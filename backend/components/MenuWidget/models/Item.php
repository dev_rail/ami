<?php

namespace backend\components\MenuWidget\models;


use yii\base\Model;
use yii\helpers\Url;

class Item extends Model
{
    public $url;
    public $name;
    public $icon;
    public $items;

    public function getActive()
    {
        $current_url = explode("/", Url::current());

        if ($this->url)
        {
            $url = explode("/", $this->urlText);

            if ($current_url[1] == $url[1])
                return "m-menu__item--active";
        }
        elseif ($this->items)
        {
            foreach ($this->items as $item)
            {
                $url = explode("/", $item->urlText);

                if ($current_url[1] == $url[1])
                    return "m-menu__item--active";

            }
        }

        return "";
    }

    public function __construct(array $config = [])
    {
        if (isset($config["items"]))
        {
            $items = [];
            foreach ($config["items"] as $item)
                $items[] = new Item($item);

            $config["items"] = $items;
        }

        parent::__construct($config);
    }

    public function getSub()
    {
        if ($this->items)
            return "m-menu__item--submenu";

        return "";
    }

    public function getClassText()
    {
        return $this->getActive()." ".$this->getSub();
    }

    public function getTogle()
    {
        if ($this->items)
            return "m-menu-submenu-toggle='hover'";

        return "";
    }

    public function getUrlText()
    {
        if ($this->url)
            return $this->url;

        return "javascript:;";
    }

    public function getAClass()
    {
        $result = 'm-menu__link';

        if ($this->items)
            $result .= " m-menu__toggle";

        return $result;
    }
}