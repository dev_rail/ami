<?php


namespace backend\controllers;


use backend\models\order\Order;
use backend\models\order\OrderStatus;

class ApiOrderController extends RestBaseController
{
    public function actionGetNew()
    {
        return ['result' => Order::find()->where(['status' => OrderStatus::STATUS_NEW])->count()];
    }
}
