<?php

namespace backend\controllers;

use common\models\settings\SmsSettings;

class SmsSettingController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = $this->getModel();

        if (\Yii::$app->request->isPost){
            if ($model->load(\Yii::$app->request->post()) && $model->validate())
                $model->save();
        }

        return $this->render('index', ['model' => $model]);
    }

    public function actionSaveSmsGetway(){
        $model = $this->getModel();
        $smsGetway = $model->smsGetway;

        if (\Yii::$app->request->isPost){
            if ($smsGetway->load(\Yii::$app->request->post()) && $smsGetway->validate())
                $smsGetway->save();
        }

        return $this->redirect('index');
    }

    private function getModel(){
        $model = SmsSettings::find()->one();

        if (!$model){
            $model = new SmsSettings(['type' => SmsSettings::TYPE_SMSC]);
            $model->save();
        }

        return $model;
    }
}
