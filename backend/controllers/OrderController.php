<?php


namespace backend\controllers;


use backend\models\forms\AddressForm;
use backend\models\forms\PhoneForm;
use backend\models\order\Order;
use backend\models\order\OrderSearchModel;
use backend\models\order\OrderStatus;
use http\Url;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class OrderController extends BaseController
{
    public function actionActive()
    {
        $orders = Order::find()->where(['status' => [OrderStatus::STATUS_NEW, OrderStatus::STATUS_COOKING]])->all();
        foreach ($orders as $order)
            $order->setCooking();

        return $this->render('active', ['orders' => $orders]);
    }

    public function actionCooked($id)
    {
        $order = $this->findModel($id);
        $order->setDelivering();

        $this->redirect(['order/active']);
    }

    public function actionNew()
    {
        $model = new PhoneForm();

        if ($model->load(\Yii::$app->request->post()) && $model->validate()){
            $hash = $model->authCustomer();
            $this->redirect(\Yii::$app->params['domens']['ami']."/index.php?hash=".$hash, 301);
        }

        return $this->render('new', ['model' => $model]);
    }

    public function actionPay($id){
        $order = $this->findModel($id);
        $order->pay();

        return $this->redirect(['view', 'id' => $order->id]);
    }

    public function actionPayIndex($id){
        $order = $this->findModel($id);
        if (!$order->paid)
            $order->pay();
        else
            $order->notPay();

        return $this->redirect(['/order/index']);
    }

    public function actionChangePay($id){
        $order = $this->findModel($id);
        $order->changeTypePay();

        return $this->redirect(['view', 'id' => $order->id]);
    }

    public function actionFinish($id){
        $order = $this->findModel($id);
        $order->finish();

        return $this->redirect(['view', 'id' => $order->id]);
    }

    public function actionIndex(){
        $model = new OrderSearchModel();

        if (\Yii::$app->request->isPost)
            $model->load(\Yii::$app->request->post());

        return $this->render('index', ['dataProvider' => $model->search(), 'model' => $model]);
    }

    public function actionView($id){
        $order = $this->findModel($id);
        $address_form = new AddressForm(['order' => $order]);

        if ($address_form->load(\Yii::$app->request->post()) && $address_form->validate())
            $address_form->save();

        if ($order->load(\Yii::$app->request->post()) && $order->validate())
            $order->save();

        return $this->render('view', ['order' => $order, 'address_form' => $address_form]);
    }

    protected function findModel($id)
    {
        $model = Order::findOne($id);

        if (!$model)
            throw new NotFoundHttpException('Данной страницы не существует!');

        return $model;
    }
}
