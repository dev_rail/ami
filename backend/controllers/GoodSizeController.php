<?php


namespace backend\controllers;

use backend\models\good\GoodSizes;
use kartik\grid\EditableColumnAction;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class GoodSizeController extends BaseController
{
    /**
     * Description all actions in one method.
     * @return array
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editrecord' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => GoodSizes::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {
                    switch ($attribute)
                    {
                        case "category_size_id":
                            return $model->categorySize->name;
                            break;
                    }

                    return $model->$attribute;
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
                'errorOptions' => ['header' => ''],
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ]
        ]);
    }

    /**
     * Deletes an existing CategorySize model.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $good = $model->good;
        $model->delete();

        if (\Yii::$app->request->isAjax)
        {
            return $this->renderAjax('/good/view', ['model' => $good]);
        }
    }

    /**
     * @param $id
     * @return GoodSizes|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = GoodSizes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}