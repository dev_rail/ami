<?php

namespace backend\controllers;


use common\models\settings\PaySettings;
use yii\web\Controller;

class PaySettingController extends Controller
{
    public function actionIndex(){
        $model = $this->getModel();

        if (\Yii::$app->request->isPost){
            if ($model->load(\Yii::$app->request->post()) && $model->validate())
                $model->save();
        }

        return $this->render('index', ['model' => $model]);
    }

    private function getModel(){
        $model = PaySettings::find()->one();

        if (!$model){
            $model = new PaySettings(['type' => PaySettings::TYPE_YANDEX]);
            $model->save();
        }

        return $model;
    }
}