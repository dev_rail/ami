<?php


namespace backend\controllers;


use backend\models\good\CategorySizes;
use backend\models\good\Goods;
use kartik\grid\EditableColumnAction;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class CategorySizeController extends BaseController
{
    /**
     * Description all actions in one method.
     * @return array
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editrecord' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => CategorySizes::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {

                    return $model->$attribute;
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
                'errorOptions' => ['header' => ''],
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ]
        ]);
    }

    /**
     * Deletes an existing CategorySize model.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $category = $model->category;
        $model->delete();

        if (\Yii::$app->request->isAjax)
        {
            return $this->renderAjax('/category/view', ['model' => $category]);
        }
    }

    /**
     * @param $id
     * @return CategorySizes|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = CategorySizes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}