<?php

namespace backend\models\promotions;


use common\models\promotions\PromotionDiscountValue;

class Promotions extends \common\models\promotions\Promotions
{
    public function afterSave($insert, $changedAttributes)
    {
        $this->createLinkData();
        parent::afterSave($insert, $changedAttributes);
    }

    private function createLinkData(){
        switch ($this->type){
            case self::TYPE_DISCOUNT:
                return $this->createLinkDiscountValue();
        }

        return false;
    }

    private function createLinkDiscountValue(){
        $model = $this->discountValue;

        if (!$model){
            $model = new PromotionDiscountValue();
            $this->link('discountValue', $model);
        }
    }
}