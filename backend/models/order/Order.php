<?php


namespace backend\models\order;


use common\models\order\Orders;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class Order extends Orders
{
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'orderCustomer.customer.phone' => 'Телефон',
            'isPayText' => 'Cтатус оплаты',
            'typePayText' => 'Тип оплаты',
            'sum' => 'Сумма',
            'statusText' => 'Статус заказа',
            'addressText' => 'Адрес'
        ]);
    }

    public function getItemsDataProvider(){
        $query = $this->getOrderItems();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }

    public function finish(){
        $this->setFinish();
        $this->finished_at = time();
        $this->save();
    }

    public function pay(){
        if ($this->type_pay == self::TYPE_PAY_CASH){
            $this->paid = true;
            $this->save();
        }
    }

    public function notPay(){
        if ($this->type_pay == self::TYPE_PAY_CASH){
            $this->paid = false;
            $this->save();
        }
    }

    public function changeTypePay(){
        if ($this->paid == true)
            return;

        switch ($this->type_pay){
            case self::TYPE_PAY_CASH:
                $this->type_pay = self::TYPE_PAY_ELECTRON;
                break;
            case self::TYPE_PAY_ELECTRON:
                $this->type_pay = self::TYPE_PAY_CASH;
                break;
        }

        $this->save();
    }

    public function getAddressText()
    {
        $order_customer = $this->orderCustomer;
        return ArrayHelper::getValue($order_customer, "customerAddress.name");
    }

    public function getOrderCustomer()
    {
        return $this->hasOne(OrderCustomer::class, ['order_id' => 'id']);
    }

    public function getCustomerPhone()
    {
        return ArrayHelper::getValue($this, "orderCustomer.customer.phone");
    }

    public function getOrderStatuses()
    {
        return $this->hasMany(OrderStatus::class, ['order_id' => 'id']);
    }

    public static function getAllPayTypes(){
        return [
            "Все",
            self::TYPE_PAY_CASH => 'Наличные',
            self::TYPE_PAY_ELECTRON => 'Картой онлайн'
        ];
    }
}
