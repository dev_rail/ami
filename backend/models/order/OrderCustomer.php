<?php


namespace backend\models\order;


use backend\models\customer\Customer;

class OrderCustomer extends \common\models\order\OrderCustomer
{
    public function getCustomer()
    {
        return $this->hasOne(Customer::class, ['id' => 'customer_id']);
    }
}
