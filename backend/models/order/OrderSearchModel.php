<?php

namespace backend\models\order;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class OrderSearchModel extends Model
{
    public $dt_start;
    public $dt_end;

    public $is_pay;
    public $type_pay;

    public function rules()
    {
        return [
            [['dt_end', 'dt_start'], 'date', 'format' => 'php: d.m.Y'],
            ['is_pay', 'boolean'],
            ['type_pay', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'is_pay' => 'Только оплаченные'
        ];
    }

    public function search(){
        $query = Order::find()->orderBy('id DESC');

        $query = $query->where(['between', 'created_at',$this->getDateStart(), $this->getDateEnd()]);

        if ($this->is_pay)
            $query = $query->andWhere(['paid' => true]);

        if ($this->type_pay)
            $query = $query->andWhere(['type_pay' => $this->type_pay]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);

        return $dataProvider;
    }

    public static function getTotalBalance($dataProvider, $fieldName){
        $totalBalance = 0;

        foreach ($dataProvider as $item){
            $totalBalance += $item[$fieldName];
        }

        return $totalBalance;
    }

    public function getDateStart(){
        if (!$this->dt_start)
            return strtotime(date("d.m.Y 00:00:00"));

        return strtotime($this->dt_start." 00:00:00");
    }

    public function getDateEnd(){
        if (!$this->dt_end)
            return strtotime(date("d.m.Y 23:59:59"));

        return strtotime($this->dt_end." 23:59:59");
    }
}