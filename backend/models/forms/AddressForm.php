<?php


namespace backend\models\forms;


use backend\models\order\Order;
use frontend\models\Addresses;
use yii\base\Model;

class AddressForm extends Model
{
    public $address;
    public $place;

    /**@var  $order Order*/
    public $order;

    public function rules()
    {
        return [
            [['address'], 'required'],
            [['address', 'place'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'address' => 'Улица, дом',
            'place' => 'Квартира/офис'
        ];
    }

    public function getCustomer(){
        return $this->order->orderCustomer->customer;
    }

    public function getOrderCustomer(){
        return $this->order->orderCustomer;
    }

    public function save(){
        $customer = $this->getCustomer();
        $address_text = "г. Казань, ул. " . $this->address;
        $address = Addresses::getAddress($address_text);

        $cus_address = $customer->addAddress($address->id, $this->place);

        $order_customer = $this->getOrderCustomer();
        $order_customer->customer_address_id = $cus_address->id;
        $order_customer->address_id = $address->id;

        $order_customer->save();
    }

    public function init()
    {
        parent::init();
        $this->initAddress();
    }

    private function initAddress()
    {
        $order = $this->order;
        $order_customer = $order->orderCustomer;
        $customer_address = $order_customer->customerAddress;

        if ($customer_address){
            $this->address = $customer_address->getStreetAndHomeText();
            $this->place = $customer_address->getPlaceText();
        }
    }
}