<?php


namespace backend\models\forms;


use backend\models\customer\Customer;
use backend\models\order\Order;
use backend\models\order\OrderCustomer;
use yii\base\Model;

class PhoneForm extends Model
{
    public $phone;

    public function rules()
    {
        return [
            ['phone', 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Телефон'
        ];
    }

    public function getPhoneValue()
    {
        $phone = $this->phone;

        $phone = str_replace(" ", "", $phone);

        return $phone;
    }

    public function authCustomer()
    {
        $customer = Customer::findOne(['phone' => $this->getPhoneValue()]);

        if (!$customer){
            $customer = new Customer(['phone' => $this->getPhoneValue()]);
            $customer->save();
        }

        $customer->generateHash();

        return $customer->hash;
    }
}