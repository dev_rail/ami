<?php


namespace backend\models\good;


use yii\data\ActiveDataProvider;

class Categories extends \common\models\good\Categories
{
    public function getCategoryGoods()
    {
        return $this->hasMany(CategoryGood::class, ['category_id' => 'id']);
    }

    public function getGoods()
    {
        return $this->hasMany(Goods::class, ['id' => 'good_id'])->via('categoryGoods');
    }

    public function getSizes()
    {
        return $this->hasMany(CategorySizes::class, ['category_id' => 'id']);
    }

    public function getSizesDataProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->getSizes(),
            'pagination' => false
        ]);
    }

    public function addSize()
    {
        $model = new CategorySizes();
        $this->link('sizes', $model);
    }

    public function addGood(Goods $good)
    {
        $this->link('goods', $good);
    }

    public function isSize()
    {
        if ($this->getSizes()->count() > 0)
            return true;
        else
            return false;
    }
}