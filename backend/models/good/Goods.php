<?php


namespace backend\models\good;


use common\models\Files;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class Goods extends \common\models\good\Goods
{
    public $image;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['image', 'file', 'extensions' => 'jpg, png, jpeg']
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'image' => 'Фото'
        ]);
    }

    public function getCategoryGood()
    {
        return $this->hasOne(CategoryGood::class, ['good_id' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id'])->via('categoryGood');
    }

    public function getSizes()
    {
        return $this->hasMany(GoodSizes::class, ['good_id' => 'id']);
    }

    public function getSizesDataProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->getSizes(),
            'pagination' => false
        ]);
    }

    public function getPhotoSrc()
    {
        $src = ArrayHelper::getValue($this, "goodPhoto.file.src");
        return $src;
    }

    public function getCategorySizesArray()
    {
        $category_sizes = $this->category->getSizes();

        $category_sizes = $category_sizes->all();

        return ArrayHelper::map($category_sizes, "id", "name");
    }

    public function addSize()
    {
        $model = new GoodSizes();
        $this->link('sizes', $model);
    }

    public function getGoodPhoto()
    {
        return $this->hasOne(GoodPhotos::class, ['good_id' => 'id']);
    }

    public function uploadImage()
    {
        $this->image = UploadedFile::getInstance($this, 'image');

        if (!$this->image)
            return false;

        if (!$this->validate())
            return false;

        $file = "/files/goods/".uniqid("f").".".$this->image->extension;
        $this->image->saveAs(Yii::getAlias('@common/web').$file);

        $files = new Files([
            'src' => $file
        ]);

        $files->save();

        $good_photo = $this->goodPhoto;

        if ($good_photo)
            $good_photo->delete();

        $good_photo = new GoodPhotos([
            'file_id' => $files->id
        ]);

        $this->link('goodPhoto', $good_photo);
    }
}