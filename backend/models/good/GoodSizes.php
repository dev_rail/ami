<?php


namespace backend\models\good;


class GoodSizes extends \common\models\good\GoodSizes
{
    public function getCategorySize()
    {
        return $this->hasOne(CategorySizes::class, ['id' => 'category_size_id']);
    }

    public function getGood()
    {
        return $this->hasOne(Goods::class, ['id' => 'good_id']);
    }
}