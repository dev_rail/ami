<?php


namespace backend\models\good;


class CategorySizes extends \common\models\good\CategorySizes
{
    const UNITS = [
        'см' => 'см',
        'л' => 'л'
    ];

    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id']);
    }

    public function getName()
    {
        return $this->size." ".$this->unit;
    }
}