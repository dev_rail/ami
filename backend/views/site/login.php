<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-login__signin">
    <div class="m-login__head">
        <h3 class="m-login__title"><?=$this->title?></h3>
    </div>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'm-login__form m-form']]); ?>
    <div class="form-group m-form__group">
        <?= $form->field($model, 'username', ['template' => '{input}{error}'])->textInput(['autofocus' => true, 'placeholder' => 'Логин', 'class' => 'form-control m-input'])?>
    </div>
    <div class="form-group m-form__group">
        <?= $form->field($model, 'password', ['template' => '{input}{error}'])->passwordInput(['placeholder' => 'Пароль', 'class' => 'form-control m-input m-login__form-input--last']) ?>
    </div>
    <?= $form->field($model, 'rememberMe', ['template' => '{input}'])->checkbox() ?>
    <div class="m-login__form-action">
        <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">Войти</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>