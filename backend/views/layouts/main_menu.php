<!-- BEGIN: Aside Menu -->
<?=\backend\components\MenuWidget\Menu::widget([
    'items' => [
        [
            'icon' => 'flaticon-gift',
            'name' => 'Продукт',
            'items' => [
                [
                    'name' => 'Категории',
                    'url' => \yii\helpers\Url::to(['category/index'])
                ]
            ]
        ],
        [
            'icon' => 'flaticon-shopping-basket',
            'name' => 'Заказы',
            'items' => [
                [
                    'name' => 'Новый',
                    'url' => \yii\helpers\Url::to(['/order/new'])
                ],
                [
                    'name' => 'Все',
                    'url' => \yii\helpers\Url::to(['/order'])
                ],
                [
                    'name' => 'Активные',
                    'url' => \yii\helpers\Url::to(['/order/active'])
                ]
            ]
        ],
        [
            'icon' => 'flaticon-star',
            'name' => 'Промо',
            'url' => \yii\helpers\Url::to(['/promotions'])
        ],
        [
            'icon' => 'flaticon-settings',
            'name' => 'Настройки',
            'items' => [
                [
                    'name' => 'СМС',
                    'url' => \yii\helpers\Url::to(['/sms-setting'])
                ],
                [
                    'name' => 'Онлайн оплата',
                    'url' => \yii\helpers\Url::to(['/pay-setting'])
                ]
            ]
        ],
    ]
])?>
<!-- END: Aside Menu -->
