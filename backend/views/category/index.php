<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'icon',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{goods} {view} {update} {delete}',
                'buttons' => [
                    'goods' => function($url, $model, $key){
                        return Html::a('<span class="glyphicon glyphicon-gift"></span>', \yii\helpers\Url::to(['/good', 'id' => $model->id]), ['title' => 'Продукты']);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
