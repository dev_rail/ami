<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\good\Categories */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$this->registerJs(
    '$("document").ready(function(){
            $("#pjax-add-category-size").on("pjax:end", function() {
                $.pjax.reload({container:"#pjax-grid-category-sizes"});
            });
    });'
);

?>
<div class="categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Продукция', ['/good', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'icon',
        ],
    ]) ?>

    <hr>

    <h2>Размеры</h2>

    <div class="gridview-editable">
        <?Pjax::begin(['id' => 'pjax-add-category-size'])?>
            <? $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
                <?=Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Добавить', ['class' => 'btn btn-success', 'name' => 'create-category-size', 'value' => 1])?>
            <? ActiveForm::end();?>
        <?Pjax::end()?>

        <?=\kartik\grid\GridView::widget([
            'dataProvider' => $model->sizesDataProvider,
            'columns' => [
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'size',
                    'editableOptions'=> [
                        'formOptions' => ['action' => ['/category-size/editrecord']]
                    ]
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'unit',
                    'editableOptions'=> [
                        'formOptions' => ['action' => ['/category-size/editrecord']],
                        'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                        'options' => [
                            'data' => \backend\models\good\CategorySizes::UNITS
                        ]
                    ]
                ],
                [
                    'class' => \kartik\grid\ActionColumn::class,
                    'template' => '{delete}',
                    'controller' => 'category-size',
                    'buttons' => [
                        'delete' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'data-pjax' => 'pjax-container',//pjax
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'POST'
                            ]);
                        },
                    ]
                ]
            ],
            'pjax'=>true,
            'pjaxSettings'=>[
                'neverTimeout'=>true,
                'options' => [
                    'id' => 'pjax-grid-category-sizes'
                ]
            ],
            'panelBeforeTemplate' => '{before}'
        ])?>
    </div>

</div>
