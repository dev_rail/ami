<?php

/* @var $this \yii\web\View */
/* @var $model array|\common\models\settings\PaySettings|null|\yii\db\ActiveRecord */
$this->title = 'Настройка Онлайн Оплаты';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="sms-setting-update">

    <h1><?=\yii\helpers\Html::encode($this->title) ?></h1>

    <?$form = \yii\widgets\ActiveForm::begin()?>
    <?=$form->field($model, 'type')->widget(\kartik\select2\Select2::class, [
        'data' => \common\models\settings\PaySettings::allTypes()
    ])?>
    <?=\yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    <? \yii\widgets\ActiveForm::end()?>

</div>
