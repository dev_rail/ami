<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\promotions\Promotions */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Промоакции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="promotions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'code',
            'startDateText',
            'endDateText',
            'type',
            'description',
        ],
    ]) ?>

    <hr>

    <?switch ($model->type){
        case \backend\models\promotions\Promotions::TYPE_DISCOUNT:
            echo $this->render('discount-value', ['model' => $model->discountValue]);
            break;
        default:
            break;
    }?>
</div>
