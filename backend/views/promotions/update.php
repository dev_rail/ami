<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\promotions\Promotions */

$this->title = 'Изменение промоакции: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Промоакции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="promotions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
