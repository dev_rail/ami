<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\promotions\Promotions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promotions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, "code")?>

    <?= $form->field($model, 'startDateText')->widget(\kartik\date\DatePicker::className()) ?>

    <?= $form->field($model, 'endDateText')->widget(\kartik\date\DatePicker::className()) ?>

    <?= $form->field($model, 'type')->widget(\kartik\select2\Select2::className(), [
            'data' => \backend\models\promotions\Promotions::getAllTypes()
    ]) ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
