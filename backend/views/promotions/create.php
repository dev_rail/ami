<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\promotions\Promotions */

$this->title = 'Новая промоакция';
$this->params['breadcrumbs'][] = ['label' => 'Промоакции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
