<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\good\Goods */

$this->title = 'Настройка отправки СМС';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?$form = \yii\widgets\ActiveForm::begin()?>
        <?=$form->field($model, 'type')->widget(\kartik\select2\Select2::class, [
            'data' => \common\models\settings\SmsSettings::allTypes()
        ])?>
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    <? \yii\widgets\ActiveForm::end()?>

    <?if ($model->type == \common\models\settings\SmsSettings::TYPE_SMSGETWAY):?>
        <?=$this->render('smsgateway', ['model' => $model->smsGetway])?>
    <?endif;?>

</div>
