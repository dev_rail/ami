<?php
$this->title = "Новый заказ";

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput; ?>

<div>
    <h3><?=$this->title?></h3>

    <?$form = ActiveForm::begin(['method' => 'POST'])?>
        <?=$form->field($model, "phone")->widget(MaskedInput::className(), ['mask' => '+7 999 999 9999'])?>
        <?=Html::submitButton('Продолжить', ['class' => 'btn btn-success'])?>
    <? ActiveForm::end()?>
</div>
