<?php

/* @var $this \yii\web\View */
/* @var $orders \backend\models\order\Order[] */

$this->title = "Активные заказы";

$script = <<<JS
    function soundClick() {
        var audio = new Audio(); // Создаём новый элемент Audio
        audio.src = '/sounds/sound.mp3'; // Указываем путь к звуку "клика"
        audio.autoplay = true; // Автоматически запускаем
    }
    
    $(document).ready(function() {
        setInterval(function() {
            $.post('/api-order/get-new', function(data) {
                if (data.result != "0"){
                    $.pjax.reload({container:"#pjax-orders"});
                    soundClick();
                }
            });
        }, 3000);
    });
JS;

$this->registerJs($script);

?>

<div>
    <h3><?=$this->title?></h3>

    <? \yii\widgets\Pjax::begin(['id' => 'pjax-orders'])?>

        <?foreach ($orders as $order):?>
            <div class="col-sm-4">
                <div class="card mb-3">
                    <div class="card-header">
                        Заказ №<?=$order->id?>
                    </div>
                    <div class="card-body">
                       <table class="table">
                           <?foreach ($order->orderItems as $item):?>
                               <tr>
                                   <td><b><?=$item->goodFullName?></b></td>
                                   <td><b><?=$item->count?></b></td>
                               </tr>
                           <?endforeach;?>
                       </table>
                        <?=\yii\helpers\Html::a('Готово', \yii\helpers\Url::to(['order/cooked', 'id' => $order->id]), ['class' => 'btn btn-success', 'style' => 'width: 100%;'])?>
                    </div>
                </div>
            </div>


        <?endforeach;?>

    <? \yii\widgets\Pjax::end()?>
</div>
