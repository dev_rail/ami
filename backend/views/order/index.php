<?php
$this->title = 'Заказы';

use yii\grid\ActionColumn;
use yii\grid\GridView; ?>

<div>
    <?$form = \yii\widgets\ActiveForm::begin()?>
        <table class="table">
            <tr>
                <td><?=$form->field($model, "dt_start")->widget(\kartik\date\DatePicker::class, ['options' => ['placeholder' => 'Начало']])->label(false)?></td>
                <td><?=$form->field($model, "dt_end")->widget(\kartik\date\DatePicker::class, ['options' => ['placeholder' => 'Конец']])->label(false)?></td>
                <td><?=$form->field($model, 'type_pay')->widget(\kartik\select2\Select2::class,
                        ['data' => \backend\models\order\Order::getAllPayTypes(), 'options' => ['placeholder' => 'Тип оплаты']])->label(false)?></td>
                <td><?=$form->field($model, 'is_pay')->checkbox()?></td>
                <td><?=\yii\helpers\Html::submitButton('Сформировать', ['class' => 'btn btn-success'])?></td>
            </tr>
        </table>
    <? \yii\widgets\ActiveForm::end()?>
    <hr>
    <h3><?=$this->title?></h3>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'columns' => [
            'id',
            'dateCreateText',
            'statusText',
            'orderCustomer.customer.phone',
            'addressText',
            'typePayText',
            'isPayText',
            [
                'attribute' => 'sum',
                'footer' => "<b>".\backend\models\order\OrderSearchModel::getTotalBalance($dataProvider->models, "sum")."</b>"
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{pay}',
                'buttons' => [
                    'pay' => function($url, $model){
                        $text = (!$model->paid)?"Оплачен":"Не оплачен";
                        $class = (!$model->paid)?"success":"danger";
                        return \yii\helpers\Html::a($text, ['/order/pay-index', 'id' => $model->id], ['class' => 'btn btn-'.$class]);
                    }
                ],
                'visibleButtons' => [
                    'pay' => function ($model, $key, $index){
                        return ($model->type_pay == \common\models\order\Orders::TYPE_PAY_CASH)?true:false;
                    }
                ]
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{view}'
            ]
        ]
    ])?>
</div>
