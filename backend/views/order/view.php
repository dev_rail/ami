<?php
$this->title = "Заказ №".$order->id;

use backend\models\order\Order;
use common\models\order\OrderStatuses;
use yii\bootstrap\Tabs;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\widgets\Pjax; ?>

<div>
    <h3><?=$this->title?></h3>
    <?if ($order->status != OrderStatuses::STATUS_COMPLETE):?>
        <hr>
        <h3>
            Действия
        </h3>
        <?if (!$order->paid):?>
            <?=Html::a('Сменить тип оплаты',['/order/change-pay', 'id' => $order->id], ['class' => 'btn btn-success '])?>
        <?endif;?>
        <?if ($order->type_pay == Order::TYPE_PAY_CASH && !$order->paid):?>
            <?=Html::a('Оплачен',['/order/pay', 'id' => $order->id], ['class' => 'btn btn-success '])?>
        <?endif;?>
        <?=Html::a('Завершен',['/order/finish', 'id' => $order->id], ['class' => 'btn btn-success '])?>
    <?endif;?>
    <hr>
    <?=DetailView::widget([
        'model' => $order,
        'attributes' => [
            'dateCreateText',
            'statusText',
            'orderCustomer.customer.phone',
            'typePayText',
            'isPayText',
            'sum',
            'description'
        ]
    ])?>

    <hr>

    <?$form = ActiveForm::begin(['method' => 'POST'])?>
        <?=$form->field($order, 'sum')?>
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    <? ActiveForm::end()?>

    <hr>

    <h3>Адрес</h3>
    <?$form = ActiveForm::begin(['method' => 'POST'])?>
        <?=$form->field($address_form, 'address')?>
        <?=$form->field($address_form, 'place')?>
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    <? ActiveForm::end()?>

    <hr>

    <h3>Позиции</h3>
    <div class="gridview-editable">
        <?=GridView::widget([
            'dataProvider' => $order->itemsDataProvider,
            'columns' => [
                'goodFullName',
                'count',
                'price',
                'sum'
            ]
        ])?>
    </div>
</div>
