<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $category \backend\models\good\Categories*/

$this->title = 'Продукция категории "'.$category->name.'"';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['/category/index']];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['/category/view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create', 'id' => $category->id], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'name',
            'description',
            'price',
            [
                'attribute' => "image",
                'format' => 'raw',
                'value' => function($model){
                    if ($model->photoSrc)
                        return Html::img($model->photoSrc, ['style' => 'width:150px;']);
                }
            ],
            'sort',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
