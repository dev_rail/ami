<?php

use backend\models\good\Goods;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\good\Goods */
/* @var $category \backend\models\good\Categories */

$this->title = 'Новая продукция в категории "'.$category->name.'"';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['/category/index']];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['/category/view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = ['label' => 'Продукции', 'url' => ['index', 'id' => $category->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="goods-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
