<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\good\Goods */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['/category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['/category/view', 'id' => $model->category->id]];
$this->params['breadcrumbs'][] = ['label' => 'Продукции', 'url' => ['index', 'id' => $model->category->id]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$this->registerJs(
    '$("document").ready(function(){
            $("#pjax-add-good-size").on("pjax:end", function() {
                $.pjax.reload({container:"#pjax-grid-good-sizes"});
            });
    });'
);
?>
<div class="goods-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description',
            'price',
            'is_combo',
            [
                'attribute' => 'image',
                'value' => \yii\helpers\ArrayHelper::getValue($model, "goodPhoto.file.src"),
                'format' => ['image',['width'=>'200','height'=>'200']],
            ],
        ],
    ]) ?>

    <hr>

    <?if ($model->category->isSize()):?>
        <h2>Размеры</h2>

        <div class="gridview-editable">
            <?Pjax::begin(['id' => 'pjax-add-good-size'])?>
                <? $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
                    <?=Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Добавить', ['class' => 'btn btn-success', 'name' => 'create-good-size', 'value' => 1])?>
                <? ActiveForm::end();?>
            <?Pjax::end()?>

            <?=\kartik\grid\GridView::widget([
                'dataProvider' => $model->sizesDataProvider,
                'columns' => [
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'category_size_id',
                        'editableOptions'=> [
                            'formOptions' => ['action' => ['/good-size/editrecord']],
                            'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                            'options' => [
                                'data' => $model->getCategorySizesArray()
                            ]
                        ],
                        'value' => function($model){
                            return \yii\helpers\ArrayHelper::getValue($model, "categorySize.name");
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'weight',
                        'editableOptions'=> [
                            'formOptions' => ['action' => ['/good-size/editrecord']],
                        ]
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'price',
                        'editableOptions'=> [
                            'formOptions' => ['action' => ['/good-size/editrecord']],
                        ]
                    ],
                    [
                        'class' => \kartik\grid\ActionColumn::class,
                        'template' => '{delete}',
                        'controller' => 'good-size',
                        'buttons' => [
                            'delete' => function ($url) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'aria-label' => Yii::t('yii', 'Delete'),
                                    'data-pjax' => 'pjax-container',//pjax
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'POST'
                                ]);
                            },
                        ]
                    ]
                ],
                'pjax'=>true,
                'pjaxSettings'=>[
                    'neverTimeout'=>true,
                    'options' => [
                        'id' => 'pjax-grid-good-sizes'
                    ]
                ],
                'panelBeforeTemplate' => '{before}'
            ])?>
        </div>
    <?endif;?>

</div>
