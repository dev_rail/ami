<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\good\Goods */

$this->title = 'Изменение продукции: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['/category/index']];
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['/category/view', 'id' => $model->category->id]];
$this->params['breadcrumbs'][] = ['label' => 'Продукции', 'url' => ['index', 'id' => $model->category->id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="goods-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
