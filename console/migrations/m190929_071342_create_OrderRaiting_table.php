<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%OrderRaiting}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Orders}}`
 */
class m190929_071342_create_OrderRaiting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%OrderRaiting}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'raiting_product' => $this->decimal(10,2),
            'raiting_delivery' => $this->decimal(10, 2),
            'description' => $this->text(),
            'is_send' => $this->boolean()->defaultValue(false),
            'is_processed' => $this->boolean()->defaultValue(false)
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-OrderRaiting-order_id}}',
            '{{%OrderRaiting}}',
            'order_id'
        );

        // add foreign key for table `{{%Orders}}`
        $this->addForeignKey(
            '{{%fk-OrderRaiting-order_id}}',
            '{{%OrderRaiting}}',
            'order_id',
            '{{%Orders}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Orders}}`
        $this->dropForeignKey(
            '{{%fk-OrderRaiting-order_id}}',
            '{{%OrderRaiting}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-OrderRaiting-order_id}}',
            '{{%OrderRaiting}}'
        );

        $this->dropTable('{{%OrderRaiting}}');
    }
}
