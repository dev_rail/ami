<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%OrderPay}}`.
 */
class m191006_040140_add_type_column_to_OrderPay_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%OrderPay}}', 'type', $this->integer()->defaultValue(\common\models\settings\PaySettings::TYPE_YANDEX));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%OrderPay}}', 'type');
    }
}
