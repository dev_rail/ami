<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%CategorySizes}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Categories}}`
 */
class m190502_084534_create_CategorySizes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%CategorySizes}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'size' => $this->decimal(10,2),
            'unit' => $this->string(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-CategorySizes-category_id}}',
            '{{%CategorySizes}}',
            'category_id'
        );

        // add foreign key for table `{{%Categories}}`
        $this->addForeignKey(
            '{{%fk-CategorySizes-category_id}}',
            '{{%CategorySizes}}',
            'category_id',
            '{{%Categories}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Categories}}`
        $this->dropForeignKey(
            '{{%fk-CategorySizes-category_id}}',
            '{{%CategorySizes}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-CategorySizes-category_id}}',
            '{{%CategorySizes}}'
        );

        $this->dropTable('{{%CategorySizes}}');
    }
}
