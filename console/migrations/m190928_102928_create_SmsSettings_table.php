<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%SmsSettings}}`.
 */
class m190928_102928_create_SmsSettings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%SmsSettings}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%SmsSettings}}');
    }
}
