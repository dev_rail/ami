<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%PromotionDiscountValue}}`.
 */
class m190914_125042_create_PromotionDiscountValue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%PromotionDiscountValue}}', [
            'id' => $this->primaryKey(),
            'promotion_id' => $this->integer(),
            'value' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%PromotionDiscountValue}}');
    }
}
