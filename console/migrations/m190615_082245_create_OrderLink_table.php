<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%OrderLink}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Orders}}`
 */
class m190615_082245_create_OrderLink_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%OrderLink}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'code' => $this->string(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-OrderLink-order_id}}',
            '{{%OrderLink}}',
            'order_id'
        );

        // add foreign key for table `{{%Orders}}`
        $this->addForeignKey(
            '{{%fk-OrderLink-order_id}}',
            '{{%OrderLink}}',
            'order_id',
            '{{%Orders}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Orders}}`
        $this->dropForeignKey(
            '{{%fk-OrderLink-order_id}}',
            '{{%OrderLink}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-OrderLink-order_id}}',
            '{{%OrderLink}}'
        );

        $this->dropTable('{{%OrderLink}}');
    }
}
