<?php

use yii\db\Migration;

/**
 * Handles adding status to table `{{%Orders}}`.
 */
class m190616_133932_add_status_column_to_Orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Orders}}', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Orders}}', 'status');
    }
}
