<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Categories}}`.
 */
class m190502_055658_create_Categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'icon' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%Categories}}');
    }
}
