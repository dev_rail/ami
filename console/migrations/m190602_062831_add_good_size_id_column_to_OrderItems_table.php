<?php

use yii\db\Migration;

/**
 * Handles adding good_size_id to table `{{%OrderItems}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%GoodSizes}}`
 */
class m190602_062831_add_good_size_id_column_to_OrderItems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%OrderItems}}', 'good_size_id', $this->integer());

        // creates index for column `good_size_id`
        $this->createIndex(
            '{{%idx-OrderItems-good_size_id}}',
            '{{%OrderItems}}',
            'good_size_id'
        );

        // add foreign key for table `{{%GoodSizes}}`
        $this->addForeignKey(
            '{{%fk-OrderItems-good_size_id}}',
            '{{%OrderItems}}',
            'good_size_id',
            '{{%GoodSizes}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%GoodSizes}}`
        $this->dropForeignKey(
            '{{%fk-OrderItems-good_size_id}}',
            '{{%OrderItems}}'
        );

        // drops index for column `good_size_id`
        $this->dropIndex(
            '{{%idx-OrderItems-good_size_id}}',
            '{{%OrderItems}}'
        );

        $this->dropColumn('{{%OrderItems}}', 'good_size_id');
    }
}
