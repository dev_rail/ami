<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%OrderStatuses}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Orders}}`
 */
class m190424_175021_create_OrderStatuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%OrderStatuses}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'date' => $this->integer(),
            'status' => $this->integer(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-OrderStatuses-order_id}}',
            '{{%OrderStatuses}}',
            'order_id'
        );

        // add foreign key for table `{{%Orders}}`
        $this->addForeignKey(
            '{{%fk-OrderStatuses-order_id}}',
            '{{%OrderStatuses}}',
            'order_id',
            '{{%Orders}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Orders}}`
        $this->dropForeignKey(
            '{{%fk-OrderStatuses-order_id}}',
            '{{%OrderStatuses}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-OrderStatuses-order_id}}',
            '{{%OrderStatuses}}'
        );

        $this->dropTable('{{%OrderStatuses}}');
    }
}
