<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Objects}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Addresses}}`
 */
class m190424_171820_create_Objects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Objects}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address_id' => $this->integer(),
        ]);

        // creates index for column `address_id`
        $this->createIndex(
            '{{%idx-Objects-address_id}}',
            '{{%Objects}}',
            'address_id'
        );

        // add foreign key for table `{{%Addresses}}`
        $this->addForeignKey(
            '{{%fk-Objects-address_id}}',
            '{{%Objects}}',
            'address_id',
            '{{%Addresses}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Addresses}}`
        $this->dropForeignKey(
            '{{%fk-Objects-address_id}}',
            '{{%Objects}}'
        );

        // drops index for column `address_id`
        $this->dropIndex(
            '{{%idx-Objects-address_id}}',
            '{{%Objects}}'
        );

        $this->dropTable('{{%Objects}}');
    }
}
