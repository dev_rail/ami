<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%SmsSettingSmsGetway}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%SmsSettings}}`
 */
class m190928_103302_create_SmsSettingSmsGetway_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%SmsSettingSmsGetway}}', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(),
            'uid' => $this->integer(),
            'sms_setting_id' => $this->integer(),
        ]);

        // creates index for column `sms_setting_id`
        $this->createIndex(
            '{{%idx-SmsSettingSmsGetway-sms_setting_id}}',
            '{{%SmsSettingSmsGetway}}',
            'sms_setting_id'
        );

        // add foreign key for table `{{%SmsSettings}}`
        $this->addForeignKey(
            '{{%fk-SmsSettingSmsGetway-sms_setting_id}}',
            '{{%SmsSettingSmsGetway}}',
            'sms_setting_id',
            '{{%SmsSettings}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%SmsSettings}}`
        $this->dropForeignKey(
            '{{%fk-SmsSettingSmsGetway-sms_setting_id}}',
            '{{%SmsSettingSmsGetway}}'
        );

        // drops index for column `sms_setting_id`
        $this->dropIndex(
            '{{%idx-SmsSettingSmsGetway-sms_setting_id}}',
            '{{%SmsSettingSmsGetway}}'
        );

        $this->dropTable('{{%SmsSettingSmsGetway}}');
    }
}
