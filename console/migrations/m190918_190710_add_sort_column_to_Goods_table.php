<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%Goods}}`.
 */
class m190918_190710_add_sort_column_to_Goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Goods}}', 'sort', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Goods}}', 'sort');
    }
}
