<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%OrderStatuses}}`.
 */
class m190928_204117_add_telegram_user_column_to_OrderStatuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%OrderStatuses}}', 'telegram_user', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%OrderStatuses}}', 'telegram_user');
    }
}
