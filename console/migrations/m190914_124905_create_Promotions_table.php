<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Promotions}}`.
 */
class m190914_124905_create_Promotions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Promotions}}', [
            'id' => $this->primaryKey(),
            'start_date' => $this->integer(),
            'end_date' => $this->integer(),
            'name' => $this->string(),
            'type' => $this->integer(),
            'description' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%Promotions}}');
    }
}
