<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `{{%Users}}`.
 */
class m190501_122507_add_updated_at_column_to_Users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Users}}', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Users}}', 'updated_at');
    }
}
