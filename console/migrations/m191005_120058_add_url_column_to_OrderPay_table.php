<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%OrderPay}}`.
 */
class m191005_120058_add_url_column_to_OrderPay_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%OrderPay}}', 'url', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%OrderPay}}', 'url');
    }
}
