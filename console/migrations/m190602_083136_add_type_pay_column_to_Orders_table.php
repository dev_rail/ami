<?php

use yii\db\Migration;

/**
 * Handles adding type_pay to table `{{%Orders}}`.
 */
class m190602_083136_add_type_pay_column_to_Orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Orders}}', 'type_pay', $this->integer());
        $this->addColumn('{{%Orders}}', 'paid', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%Orders}}', 'finished_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Orders}}', 'type_pay');
        $this->dropColumn('{{%Orders}}', 'paid');
        $this->dropColumn('{{%Orders}}', 'finished_at');
    }
}
