<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%PhoneConfirmations}}`.
 */
class m190518_093248_create_PhoneConfirmations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%PhoneConfirmations}}', [
            'id' => $this->primaryKey(),
            'phone' => $this->string(),
            'session_id' => $this->string(),
            'code' => $this->integer(),
            'attempts' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%PhoneConfirmations}}');
    }
}
