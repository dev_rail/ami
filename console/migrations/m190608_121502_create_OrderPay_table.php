<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%OrderPay}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Orders}}`
 */
class m190608_121502_create_OrderPay_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%OrderPay}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'pay_id' => $this->string(),
            'is_success' => $this->boolean(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-OrderPay-order_id}}',
            '{{%OrderPay}}',
            'order_id'
        );

        // add foreign key for table `{{%Orders}}`
        $this->addForeignKey(
            '{{%fk-OrderPay-order_id}}',
            '{{%OrderPay}}',
            'order_id',
            '{{%Orders}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Orders}}`
        $this->dropForeignKey(
            '{{%fk-OrderPay-order_id}}',
            '{{%OrderPay}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-OrderPay-order_id}}',
            '{{%OrderPay}}'
        );

        $this->dropTable('{{%OrderPay}}');
    }
}
