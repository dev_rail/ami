<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Compositions}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Goods}}`
 * - `{{%Products}}`
 */
class m190424_174321_create_Compositions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Compositions}}', [
            'id' => $this->primaryKey(),
            'good_id' => $this->integer(),
            'product_id' => $this->integer(),
            'amt' => $this->decimal(10,2),
        ]);

        // creates index for column `goods_id`
        $this->createIndex(
            '{{%idx-Compositions-goods_id}}',
            '{{%Compositions}}',
            'good_id'
        );

        // add foreign key for table `{{%Goods}}`
        $this->addForeignKey(
            '{{%fk-Compositions-goods_id}}',
            '{{%Compositions}}',
            'good_id',
            '{{%Goods}}',
            'id'
        );

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-Compositions-product_id}}',
            '{{%Compositions}}',
            'product_id'
        );

        // add foreign key for table `{{%Products}}`
        $this->addForeignKey(
            '{{%fk-Compositions-product_id}}',
            '{{%Compositions}}',
            'product_id',
            '{{%Products}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Goods}}`
        $this->dropForeignKey(
            '{{%fk-Compositions-goods_id}}',
            '{{%Compositions}}'
        );

        // drops index for column `goods_id`
        $this->dropIndex(
            '{{%idx-Compositions-goods_id}}',
            '{{%Compositions}}'
        );

        // drops foreign key for table `{{%Products}}`
        $this->dropForeignKey(
            '{{%fk-Compositions-product_id}}',
            '{{%Compositions}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-Compositions-product_id}}',
            '{{%Compositions}}'
        );

        $this->dropTable('{{%Compositions}}');
    }
}
