<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%OrderPromotion}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Orders}}`
 * - `{{%Promotions}}`
 */
class m190921_174048_create_OrderPromotion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%OrderPromotion}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'promotion_id' => $this->integer(),
            'sum_discount' => $this->decimal(10,2),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-OrderPromotion-order_id}}',
            '{{%OrderPromotion}}',
            'order_id'
        );

        // add foreign key for table `{{%Orders}}`
        $this->addForeignKey(
            '{{%fk-OrderPromotion-order_id}}',
            '{{%OrderPromotion}}',
            'order_id',
            '{{%Orders}}',
            'id',
            'CASCADE'
        );

        // creates index for column `promotion_id`
        $this->createIndex(
            '{{%idx-OrderPromotion-promotion_id}}',
            '{{%OrderPromotion}}',
            'promotion_id'
        );

        // add foreign key for table `{{%Promotions}}`
        $this->addForeignKey(
            '{{%fk-OrderPromotion-promotion_id}}',
            '{{%OrderPromotion}}',
            'promotion_id',
            '{{%Promotions}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Orders}}`
        $this->dropForeignKey(
            '{{%fk-OrderPromotion-order_id}}',
            '{{%OrderPromotion}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-OrderPromotion-order_id}}',
            '{{%OrderPromotion}}'
        );

        // drops foreign key for table `{{%Promotions}}`
        $this->dropForeignKey(
            '{{%fk-OrderPromotion-promotion_id}}',
            '{{%OrderPromotion}}'
        );

        // drops index for column `promotion_id`
        $this->dropIndex(
            '{{%idx-OrderPromotion-promotion_id}}',
            '{{%OrderPromotion}}'
        );

        $this->dropTable('{{%OrderPromotion}}');
    }
}
