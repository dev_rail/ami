<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%Goods}}`.
 */
class m190922_112731_add_is_combo_column_to_Goods_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Goods}}', 'is_combo', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Goods}}', 'is_combo');
    }
}
