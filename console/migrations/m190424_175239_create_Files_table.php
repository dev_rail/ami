<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Files}}`.
 */
class m190424_175239_create_Files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Files}}', [
            'id' => $this->primaryKey(),
            'src' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%Files}}');
    }
}
