<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%GoodSizes}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Goods}}`
 * - `{{%CategorySizes}}`
 */
class m190502_115251_create_GoodSizes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%GoodSizes}}', [
            'id' => $this->primaryKey(),
            'good_id' => $this->integer(),
            'category_size_id' => $this->integer(),
            'price' => $this->decimal(10,2),
        ]);

        // creates index for column `good_id`
        $this->createIndex(
            '{{%idx-GoodSizes-good_id}}',
            '{{%GoodSizes}}',
            'good_id'
        );

        // add foreign key for table `{{%Goods}}`
        $this->addForeignKey(
            '{{%fk-GoodSizes-good_id}}',
            '{{%GoodSizes}}',
            'good_id',
            '{{%Goods}}',
            'id'
        );

        // creates index for column `category_size_id`
        $this->createIndex(
            '{{%idx-GoodSizes-category_size_id}}',
            '{{%GoodSizes}}',
            'category_size_id'
        );

        // add foreign key for table `{{%CategorySizes}}`
        $this->addForeignKey(
            '{{%fk-GoodSizes-category_size_id}}',
            '{{%GoodSizes}}',
            'category_size_id',
            '{{%CategorySizes}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Goods}}`
        $this->dropForeignKey(
            '{{%fk-GoodSizes-good_id}}',
            '{{%GoodSizes}}'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            '{{%idx-GoodSizes-good_id}}',
            '{{%GoodSizes}}'
        );

        // drops foreign key for table `{{%CategorySizes}}`
        $this->dropForeignKey(
            '{{%fk-GoodSizes-category_size_id}}',
            '{{%GoodSizes}}'
        );

        // drops index for column `category_size_id`
        $this->dropIndex(
            '{{%idx-GoodSizes-category_size_id}}',
            '{{%GoodSizes}}'
        );

        $this->dropTable('{{%GoodSizes}}');
    }
}
