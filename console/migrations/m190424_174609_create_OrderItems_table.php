<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%OrderItems}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Orders}}`
 * - `{{%Goods}}`
 */
class m190424_174609_create_OrderItems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%OrderItems}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'good_id' => $this->integer(),
            'count' => $this->integer(),
            'price' => $this->decimal(10,2),
            'sum' => $this->decimal(10,2),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-OrderItems-order_id}}',
            '{{%OrderItems}}',
            'order_id'
        );

        // add foreign key for table `{{%Orders}}`
        $this->addForeignKey(
            '{{%fk-OrderItems-order_id}}',
            '{{%OrderItems}}',
            'order_id',
            '{{%Orders}}',
            'id',
            'CASCADE'
        );

        // creates index for column `good_id`
        $this->createIndex(
            '{{%idx-OrderItems-good_id}}',
            '{{%OrderItems}}',
            'good_id'
        );

        // add foreign key for table `{{%Goods}}`
        $this->addForeignKey(
            '{{%fk-OrderItems-good_id}}',
            '{{%OrderItems}}',
            'good_id',
            '{{%Goods}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Orders}}`
        $this->dropForeignKey(
            '{{%fk-OrderItems-order_id}}',
            '{{%OrderItems}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-OrderItems-order_id}}',
            '{{%OrderItems}}'
        );

        // drops foreign key for table `{{%Goods}}`
        $this->dropForeignKey(
            '{{%fk-OrderItems-good_id}}',
            '{{%OrderItems}}'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            '{{%idx-OrderItems-good_id}}',
            '{{%OrderItems}}'
        );

        $this->dropTable('{{%OrderItems}}');
    }
}
