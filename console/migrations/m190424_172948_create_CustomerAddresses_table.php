<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%CustomerAddresses}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Customers}}`
 * - `{{%Addresses}}`
 */
class m190424_172948_create_CustomerAddresses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%CustomerAddresses}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer(),
            'address_id' => $this->integer(),
            'name' => $this->string(),
        ]);

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-CustomerAddresses-customer_id}}',
            '{{%CustomerAddresses}}',
            'customer_id'
        );

        // add foreign key for table `{{%Customers}}`
        $this->addForeignKey(
            '{{%fk-CustomerAddresses-customer_id}}',
            '{{%CustomerAddresses}}',
            'customer_id',
            '{{%Customers}}',
            'id'
        );

        // creates index for column `address_id`
        $this->createIndex(
            '{{%idx-CustomerAddresses-address_id}}',
            '{{%CustomerAddresses}}',
            'address_id'
        );

        // add foreign key for table `{{%Addresses}}`
        $this->addForeignKey(
            '{{%fk-CustomerAddresses-address_id}}',
            '{{%CustomerAddresses}}',
            'address_id',
            '{{%Addresses}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Customers}}`
        $this->dropForeignKey(
            '{{%fk-CustomerAddresses-customer_id}}',
            '{{%CustomerAddresses}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-CustomerAddresses-customer_id}}',
            '{{%CustomerAddresses}}'
        );

        // drops foreign key for table `{{%Addresses}}`
        $this->dropForeignKey(
            '{{%fk-CustomerAddresses-address_id}}',
            '{{%CustomerAddresses}}'
        );

        // drops index for column `address_id`
        $this->dropIndex(
            '{{%idx-CustomerAddresses-address_id}}',
            '{{%CustomerAddresses}}'
        );

        $this->dropTable('{{%CustomerAddresses}}');
    }
}
