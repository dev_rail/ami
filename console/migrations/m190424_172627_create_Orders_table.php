<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Orders}}`.
 */
class m190424_172627_create_Orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Orders}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'sum' => $this->decimal(10,2),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%Orders}}');
    }
}
