<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Products}}`.
 */
class m190424_172359_create_Products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Products}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%Products}}');
    }
}
