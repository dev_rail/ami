<?php

use yii\db\Migration;

/**
 * Handles adding place to table `{{%CustomerAddresses}}`.
 */
class m190601_143653_add_place_column_to_CustomerAddresses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%CustomerAddresses}}', 'place', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%CustomerAddresses}}', 'place');
    }
}
