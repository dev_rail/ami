<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%CategoryGood}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Categories}}`
 * - `{{%Goods}}`
 */
class m190502_055937_create_CategoryGood_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%CategoryGood}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'good_id' => $this->integer(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-CategoryGood-category_id}}',
            '{{%CategoryGood}}',
            'category_id'
        );

        // add foreign key for table `{{%Categories}}`
        $this->addForeignKey(
            '{{%fk-CategoryGood-category_id}}',
            '{{%CategoryGood}}',
            'category_id',
            '{{%Categories}}',
            'id'
        );

        // creates index for column `good_id`
        $this->createIndex(
            '{{%idx-CategoryGood-good_id}}',
            '{{%CategoryGood}}',
            'good_id'
        );

        // add foreign key for table `{{%Goods}}`
        $this->addForeignKey(
            '{{%fk-CategoryGood-good_id}}',
            '{{%CategoryGood}}',
            'good_id',
            '{{%Goods}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Categories}}`
        $this->dropForeignKey(
            '{{%fk-CategoryGood-category_id}}',
            '{{%CategoryGood}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-CategoryGood-category_id}}',
            '{{%CategoryGood}}'
        );

        // drops foreign key for table `{{%Goods}}`
        $this->dropForeignKey(
            '{{%fk-CategoryGood-good_id}}',
            '{{%CategoryGood}}'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            '{{%idx-CategoryGood-good_id}}',
            '{{%CategoryGood}}'
        );

        $this->dropTable('{{%CategoryGood}}');
    }
}
