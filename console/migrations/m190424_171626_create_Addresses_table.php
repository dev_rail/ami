<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Addresses}}`.
 */
class m190424_171626_create_Addresses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Addresses}}', [
            'id' => $this->primaryKey(),
            'address' => $this->string(),
            'lat' => $this->decimal(10,6),
            'lon' => $this->decimal(10,6),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%Addresses}}');
    }
}
