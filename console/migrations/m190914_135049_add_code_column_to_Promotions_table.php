<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%Promotions}}`.
 */
class m190914_135049_add_code_column_to_Promotions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Promotions}}', 'code', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Promotions}}', 'code');
    }
}
