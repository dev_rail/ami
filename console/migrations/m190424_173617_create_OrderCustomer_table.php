<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%OrderCustomer}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Orders}}`
 * - `{{%Customers}}`
 * - `{{%CustomerAddresses}}`
 * - `{{%Addresses}}`
 */
class m190424_173617_create_OrderCustomer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%OrderCustomer}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'customer_id' => $this->integer(),
            'customer_address_id' => $this->integer(),
            'address_id' => $this->integer(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            '{{%idx-OrderCustomer-order_id}}',
            '{{%OrderCustomer}}',
            'order_id'
        );

        // add foreign key for table `{{%Orders}}`
        $this->addForeignKey(
            '{{%fk-OrderCustomer-order_id}}',
            '{{%OrderCustomer}}',
            'order_id',
            '{{%Orders}}',
            'id'
        );

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-OrderCustomer-customer_id}}',
            '{{%OrderCustomer}}',
            'customer_id'
        );

        // add foreign key for table `{{%Customers}}`
        $this->addForeignKey(
            '{{%fk-OrderCustomer-customer_id}}',
            '{{%OrderCustomer}}',
            'customer_id',
            '{{%Customers}}',
            'id'
        );

        // creates index for column `customer_address_id`
        $this->createIndex(
            '{{%idx-OrderCustomer-customer_address_id}}',
            '{{%OrderCustomer}}',
            'customer_address_id'
        );

        // add foreign key for table `{{%CustomerAddresses}}`
        $this->addForeignKey(
            '{{%fk-OrderCustomer-customer_address_id}}',
            '{{%OrderCustomer}}',
            'customer_address_id',
            '{{%CustomerAddresses}}',
            'id'
        );

        // creates index for column `address_id`
        $this->createIndex(
            '{{%idx-OrderCustomer-address_id}}',
            '{{%OrderCustomer}}',
            'address_id'
        );

        // add foreign key for table `{{%Addresses}}`
        $this->addForeignKey(
            '{{%fk-OrderCustomer-address_id}}',
            '{{%OrderCustomer}}',
            'address_id',
            '{{%Addresses}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Orders}}`
        $this->dropForeignKey(
            '{{%fk-OrderCustomer-order_id}}',
            '{{%OrderCustomer}}'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            '{{%idx-OrderCustomer-order_id}}',
            '{{%OrderCustomer}}'
        );

        // drops foreign key for table `{{%Customers}}`
        $this->dropForeignKey(
            '{{%fk-OrderCustomer-customer_id}}',
            '{{%OrderCustomer}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-OrderCustomer-customer_id}}',
            '{{%OrderCustomer}}'
        );

        // drops foreign key for table `{{%CustomerAddresses}}`
        $this->dropForeignKey(
            '{{%fk-OrderCustomer-customer_address_id}}',
            '{{%OrderCustomer}}'
        );

        // drops index for column `customer_address_id`
        $this->dropIndex(
            '{{%idx-OrderCustomer-customer_address_id}}',
            '{{%OrderCustomer}}'
        );

        // drops foreign key for table `{{%Addresses}}`
        $this->dropForeignKey(
            '{{%fk-OrderCustomer-address_id}}',
            '{{%OrderCustomer}}'
        );

        // drops index for column `address_id`
        $this->dropIndex(
            '{{%idx-OrderCustomer-address_id}}',
            '{{%OrderCustomer}}'
        );

        $this->dropTable('{{%OrderCustomer}}');
    }
}
