<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%GoodPhotos}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Goods}}`
 * - `{{%Files}}`
 */
class m190424_175409_create_GoodPhotos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%GoodPhotos}}', [
            'id' => $this->primaryKey(),
            'good_id' => $this->integer(),
            'file_id' => $this->integer(),
        ]);

        // creates index for column `good_id`
        $this->createIndex(
            '{{%idx-GoodPhotos-good_id}}',
            '{{%GoodPhotos}}',
            'good_id'
        );

        // add foreign key for table `{{%Goods}}`
        $this->addForeignKey(
            '{{%fk-GoodPhotos-good_id}}',
            '{{%GoodPhotos}}',
            'good_id',
            '{{%Goods}}',
            'id',
            'CASCADE'
        );

        // creates index for column `file_id`
        $this->createIndex(
            '{{%idx-GoodPhotos-file_id}}',
            '{{%GoodPhotos}}',
            'file_id'
        );

        // add foreign key for table `{{%Files}}`
        $this->addForeignKey(
            '{{%fk-GoodPhotos-file_id}}',
            '{{%GoodPhotos}}',
            'file_id',
            '{{%Files}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Goods}}`
        $this->dropForeignKey(
            '{{%fk-GoodPhotos-good_id}}',
            '{{%GoodPhotos}}'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            '{{%idx-GoodPhotos-good_id}}',
            '{{%GoodPhotos}}'
        );

        // drops foreign key for table `{{%Files}}`
        $this->dropForeignKey(
            '{{%fk-GoodPhotos-file_id}}',
            '{{%GoodPhotos}}'
        );

        // drops index for column `file_id`
        $this->dropIndex(
            '{{%idx-GoodPhotos-file_id}}',
            '{{%GoodPhotos}}'
        );

        $this->dropTable('{{%GoodPhotos}}');
    }
}
