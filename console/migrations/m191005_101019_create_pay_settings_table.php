<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pay_settings}}`.
 */
class m191005_101019_create_pay_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%PaySettings}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%PaySettings}}');
    }
}
