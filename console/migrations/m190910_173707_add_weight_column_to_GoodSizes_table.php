<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%GoodSizes}}`.
 */
class m190910_173707_add_weight_column_to_GoodSizes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%GoodSizes}}', 'weight', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%GoodSizes}}', 'weight');
    }
}
