<?php

use yii\db\Migration;

/**
 * Handles adding description to table `{{%Orders}}`.
 */
class m190616_064731_add_description_column_to_Orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Orders}}', 'description', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Orders}}', 'description');
    }
}
