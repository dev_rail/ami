<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%BackSessions}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%customer}}`
 */
class m190831_112848_create_BackSessions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%BackSessions}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer(),
            'session_id' => $this->string(),
        ]);

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-BackSessions-customer_id}}',
            '{{%BackSessions}}',
            'customer_id'
        );

        // add foreign key for table `{{%customer}}`
        $this->addForeignKey(
            '{{%fk-BackSessions-customer_id}}',
            '{{%BackSessions}}',
            'customer_id',
            '{{%Customers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%customer}}`
        $this->dropForeignKey(
            '{{%fk-BackSessions-customer_id}}',
            '{{%BackSessions}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-BackSessions-customer_id}}',
            '{{%BackSessions}}'
        );

        $this->dropTable('{{%BackSessions}}');
    }
}
