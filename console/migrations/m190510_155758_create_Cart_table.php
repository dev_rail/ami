<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%Cart}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%Goods}}`
 * - `{{%GoodSizes}}`
 */
class m190510_155758_create_Cart_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%Cart}}', [
            'id' => $this->primaryKey(),
            'session_id' => $this->string(),
            'good_id' => $this->integer(),
            'good_size_id' => $this->integer(),
            'count' => $this->decimal(10,2),
        ]);

        // creates index for column `good_id`
        $this->createIndex(
            '{{%idx-Cart-good_id}}',
            '{{%Cart}}',
            'good_id'
        );

        // add foreign key for table `{{%Goods}}`
        $this->addForeignKey(
            '{{%fk-Cart-good_id}}',
            '{{%Cart}}',
            'good_id',
            '{{%Goods}}',
            'id',
            'CASCADE'
        );

        // creates index for column `good_size_id`
        $this->createIndex(
            '{{%idx-Cart-good_size_id}}',
            '{{%Cart}}',
            'good_size_id'
        );

        // add foreign key for table `{{%GoodSizes}}`
        $this->addForeignKey(
            '{{%fk-Cart-good_size_id}}',
            '{{%Cart}}',
            'good_size_id',
            '{{%GoodSizes}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%Goods}}`
        $this->dropForeignKey(
            '{{%fk-Cart-good_id}}',
            '{{%Cart}}'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            '{{%idx-Cart-good_id}}',
            '{{%Cart}}'
        );

        // drops foreign key for table `{{%GoodSizes}}`
        $this->dropForeignKey(
            '{{%fk-Cart-good_size_id}}',
            '{{%Cart}}'
        );

        // drops index for column `good_size_id`
        $this->dropIndex(
            '{{%idx-Cart-good_size_id}}',
            '{{%Cart}}'
        );

        $this->dropTable('{{%Cart}}');
    }
}
