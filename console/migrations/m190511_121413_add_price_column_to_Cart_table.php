<?php

use yii\db\Migration;

/**
 * Handles adding price to table `{{%Cart}}`.
 */
class m190511_121413_add_price_column_to_Cart_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Cart}}', 'price', $this->decimal(10,2));
        $this->addColumn('{{%Cart}}', 'sum', $this->decimal(10,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Cart}}', 'price');
        $this->dropColumn('{{%Cart}}', 'sum');
    }
}
