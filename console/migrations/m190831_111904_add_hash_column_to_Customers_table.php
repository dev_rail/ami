<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%Customers}}`.
 */
class m190831_111904_add_hash_column_to_Customers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%Customers}}', 'hash', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%Customers}}', 'hash');
    }
}
