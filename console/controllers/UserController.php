<?php


namespace console\controllers;


use common\models\Users;
use yii\console\Controller;

class UserController extends Controller
{
    public function actionAdd($username, $password)
    {
        $user = new Users([
            'username' => $username
        ]);

        $user->setPassword($password);
        $user->save();

        echo "Success \r\n";
    }
}