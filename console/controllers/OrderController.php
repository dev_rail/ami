<?php

namespace console\controllers;


use common\models\order\OrderRaiting;
use common\models\order\Orders;
use common\models\order\OrderStatuses;
use yii\console\Controller;

class OrderController extends Controller
{
    public function actionRaiting(){
        $order_raitings = OrderRaiting::find()
            ->joinWith('order')
            ->where(['is_send' => false])
            ->andWhere(['status' => OrderStatuses::STATUS_COMPLETE])
            ->andWhere(['<=', 'finished_at', time() - 60 * 20])
            ->all();



        foreach ($order_raitings as $order_raiting){
            $order = $order_raiting->order;

            if ($order){
                $order->sendRaitingSms();
            }
        }
    }
}