<?php


namespace console\controllers;


use yii\console\Controller;

class TelegramController extends Controller
{
    public function actionUpdates()
    {
        print_r(\Yii::$app->telegram->getUpdates());
    }

    public function actionSetWebhook(){
        $result = \Yii::$app->telegram->setWebhook([
            'url' => 'https://amipizza.ru/bot'
        ]);

        print_r($result);
    }
}