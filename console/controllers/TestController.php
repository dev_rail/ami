<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 24.09.19
 * Time: 20:04
 */

namespace console\controllers;


use common\models\geo\Addresses;

use yii\console\Controller;

class TestController extends Controller
{
    public function actionGeo(){

        $adr = Addresses::findOne(44);
        print_r($adr->getLocation());
    }
}