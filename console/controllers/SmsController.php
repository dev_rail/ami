<?php


namespace console\controllers;


use common\components\SmsGatewayHelper;
use common\components\SmsHelper;
use yii\console\Controller;

class SmsController extends Controller
{
    public function actionTest()
    {
        $sms = new SmsGatewayHelper();
        $sms->send("+79600506123", "test333");
    }
}
