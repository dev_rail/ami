<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'smsgateway_api' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU2NDgxOTA0OSwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjcyMjcwLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.99O7wdoU3DDdYT669QNJjdaZ8BiW0dZudwHNOKigZuM'
];
