<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'telegram' => [
            'class' => 'aki\telegram\Telegram',
            'botToken' => '701492299:AAHABnEhNPRmSf_SFWC9q0w_qg9YE2TdHA4'
        ],
        'geo' => [
            'class' => \common\components\Geo::class,
            'key' => 'AIzaSyBsQz14xRlJGXE5fDYubdE_UPf034u9wOo'
        ]
    ],
];
