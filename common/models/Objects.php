<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Objects".
 *
 * @property int $id
 * @property string $name
 * @property int $address_id
 *
 * @property Addresses $address
 */
class Objects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Objects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Addresses::className(), 'targetAttribute' => ['address_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address_id' => 'Address ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Addresses::className(), ['id' => 'address_id']);
    }
}
