<?php

namespace common\models\promotions;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Promotions".
 *
 * @property int $id
 * @property int $start_date
 * @property int $end_date
 * @property string $name
 * @property int $type
 * @property string $description
 * @property string $code
 */
class Promotions extends \yii\db\ActiveRecord
{
    const TYPE_DISCOUNT = 1;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Promotions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date', 'type'], 'integer'],
            [['name', 'description', 'code'], 'string', 'max' => 255],
            ['code', 'unique'],
            [['startDateText', 'endDateText'], 'date', 'format' => 'php: d.m.Y']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'name' => 'Наименование',
            'type' => 'Тип',
            'description' => 'Описание',
            'code' => 'Код',
            'startDateText' => 'Начало',
            'endDateText' => 'Конец',
            'typeText' => 'Тип'
        ];
    }

    public function getStartDateText(){
        if ($this->start_date)
            return date("d.m.Y", $this->start_date);

        return null;
    }

    public function getEndDateText(){
        if ($this->end_date)
            return date("d.m.Y", $this->end_date);

        return null;
    }

    public function setStartDateText($value){
        if ($value)
            $this->start_date = strtotime($value." 00:00:00");
        else
            $this->start_date = null;
    }

    public function setEndDateText($value){
        if ($value)
            $this->end_date = strtotime($value." 23:59:59");
        else
            $this->end_date = null;
    }

    static public function getAllTypes(){
        return [
            static::TYPE_DISCOUNT => 'Скидка'
        ];
    }

    public function getDiscountValue(){
        return $this->hasOne(PromotionDiscountValue::class, ['promotion_id' => 'id']);
    }

    public function getTypeText(){
        return ArrayHelper::getValue(static::getAllTypes(), $this->type);
    }
}
