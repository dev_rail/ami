<?php

namespace common\models\promotions;

use Yii;

/**
 * This is the model class for table "PromotionDiscountValue".
 *
 * @property int $id
 * @property int $promotion_id
 * @property int $value
 */
class PromotionDiscountValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PromotionDiscountValue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promotion_id', 'value'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promotion_id' => 'Promotion ID',
            'value' => 'Значение',
        ];
    }
}
