<?php

namespace common\models\customer;

use Yii;

/**
 * This is the model class for table "PhoneConfirmations".
 *
 * @property int $id
 * @property string $phone
 * @property int $code
 * @property int $attempts
 * @property string $session_id
 */
class PhoneConfirmations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PhoneConfirmations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'attempts'], 'integer'],
            [['phone', 'session_id'], 'string', 'max' => 255],
            ['phone', 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'code' => 'Code',
            'attempts' => 'Attempts',
        ];
    }
}
