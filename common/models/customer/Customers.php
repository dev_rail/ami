<?php

namespace common\models\customer;

use common\models\order\OrderCustomer;
use Yii;

/**
 * This is the model class for table "Customers".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $hash
 *
 * @property CustomerAddresses[] $customerAddresses
 * @property OrderCustomer[] $orderCustomers
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Customers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'hash'], 'string', 'max' => 255],
            ['phone', 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerAddresses()
    {
        return $this->hasMany(CustomerAddresses::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderCustomers()
    {
        return $this->hasMany(OrderCustomer::className(), ['customer_id' => 'id']);
    }

    public function addAddress($address_id, $place)
    {
        $address = $this->getCustomerAddresses()->where(['address_id' => $address_id, 'place' => $place])->one();

        if ($address)
            return $address;

        $address = new CustomerAddresses(['address_id' => $address_id, 'place' => $place, 'customer_id' => $this->id]);
        $address->name = $address->address->address."-".$place;

        $address->save();

        return $address;
    }

    public function generateHash(){
        $this->hash = md5(time().$this->phone.uniqid());
        $this->save();
    }

    public function clearHash(){
        $this->hash = null;
        $this->save();
    }
}
