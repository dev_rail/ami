<?php

namespace common\models\customer;

use common\models\geo\Addresses;
use common\models\order\OrderCustomer;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "CustomerAddresses".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $address_id
 * @property string $name
 *
 * @property Addresses $address
 * @property Customers $customer
 * @property OrderCustomer[] $orderCustomers
 * @property string $place [varchar(255)]
 */
class CustomerAddresses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CustomerAddresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'address_id'], 'integer'],
            [['name', 'place'], 'string', 'max' => 255],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Addresses::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'address_id' => 'Address ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Addresses::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderCustomers()
    {
        return $this->hasMany(OrderCustomer::className(), ['customer_address_id' => 'id']);
    }

    public function getFullAddressText()
    {
        $adr = $this->address->address." - ".$this->place;
        return $adr;
    }

    public function getStreetAndHomeText(){
        $address_array = explode(",", $this->getFullAddressText());
        $result = $address_array[1];
        $result_home_array = explode("-", ArrayHelper::getValue($address_array, 2));
        $home = $result_home_array[0];
        $result = str_replace("ул.", "", $result);
        $result = trim(trim($result), ",").", ".$home;

        return $result;
    }

    public function getPlaceText(){
        $address_array = explode(",", $this->getFullAddressText());
        $result = ArrayHelper::getValue($address_array, 2);
        $result_array = explode("-", $result);
        $result = ArrayHelper::getValue($result_array, 1);
        $result = trim($result);

        return $result;
    }
}
