<?php

namespace common\models\settings;

use Yii;

/**
 * This is the model class for table "SmsSettings".
 *
 * @property int $id
 * @property int $type
 */
class SmsSettings extends \yii\db\ActiveRecord
{
    const TYPE_SMSC = 1;
    const TYPE_SMSGETWAY = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'SmsSettings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
        ];
    }

    public static function allTypes(){
        return [
            self::TYPE_SMSC => 'Шлюз SMSC',
            self::TYPE_SMSGETWAY => 'Сервис SmsGetway'
        ];
    }

    public function getSmsSettingSmsGetway(){
        return $this->hasOne(SmsSettingSmsGetway::class, ['sms_setting_id' => 'id']);
    }

    public function getSmsGetway(){
        if ($this->smsSettingSmsGetway)
            return $this->smsSettingSmsGetway;

        $model = new SmsSettingSmsGetway([
            'sms_setting_id' => $this->id
        ]);

        $model->save();

        return $model;
    }
}
