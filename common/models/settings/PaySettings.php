<?php

namespace common\models\settings;

use Yii;

/**
 * This is the model class for table "PaySettings".
 *
 * @property int $id
 * @property int $type
 * @property string $login
 * @property string $password
 * @property string $token
 */
class PaySettings extends \yii\db\ActiveRecord
{
    const TYPE_YANDEX = 1;
    const TYPE_SBERBANK = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PaySettings';
    }

    public static function allTypes()
    {
        return [
            self::TYPE_YANDEX => 'Яндекс',
            self::TYPE_SBERBANK => 'Сбербанк'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип'
        ];
    }
}
