<?php

namespace common\models\settings;

use Yii;

/**
 * This is the model class for table "SmsSettingSmsGetway".
 *
 * @property int $id
 * @property string $phone
 * @property int $uid
 * @property int $sms_setting_id
 *
 * @property SmsSettings $smsSetting
 */
class SmsSettingSmsGetway extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'SmsSettingSmsGetway';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'sms_setting_id'], 'integer'],
            [['phone'], 'string', 'max' => 255],
            [['sms_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsSettings::className(), 'targetAttribute' => ['sms_setting_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'uid' => 'Uid',
            'sms_setting_id' => 'Sms Setting ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmsSetting()
    {
        return $this->hasOne(SmsSettings::className(), ['id' => 'sms_setting_id']);
    }
}
