<?php

namespace common\models\order;

use common\components\SberbankPay;
use common\components\YandexPay;
use common\models\settings\PaySettings;
use Yii;

/**
 * This is the model class for table "OrderPay".
 *
 * @property int $id
 * @property int $order_id
 * @property string $pay_id
 * @property int $is_success
 * @property string $url
 * @property int $type
 *
 * @property Orders $order
 */
class OrderPay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OrderPay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'type'], 'integer'],
            [['is_success'], 'boolean'],
            [['pay_id'], 'string', 'max' => 255],
            ['url', 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'pay_id' => 'Pay ID',
            'is_success' => 'Is Success',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    public function updateOrderPaid()
    {
        if (!$this->is_success)
            return false;

        if ($this->getOldAttribute('is_success') == false)
            $this->sendTelegram();

        $order = $this->order;
        $order->paid = true;
        $order->save();
    }

    public function beforeSave($insert)
    {
        $this->updateOrderPaid();
        return parent::beforeSave($insert);
    }

    private function sendTelegram()
    {
        $text = "Заказ №".$this->order->id." ОПЛАЧЕН";

        Yii::$app->telegram->sendMessage([
            'chat_id' => -273213862,
            'text' => $text
        ]);
    }

    public function getPaidOnline(){
        $result = false;
        switch ($this->type){
            case PaySettings::TYPE_YANDEX:
                $result = $this->isPayYandex();
                break;
            case PaySettings::TYPE_SBERBANK:
                $result = $this->isPaySberbank();
                break;
            default:
                $result = false;
                break;
        }

        if ($result)
            $this->setPaid();

        return $result;
    }

    private function isPayYandex()
    {
        $pay = new YandexPay();
        $payment = $pay->getStatus($this->pay_id);

        if ($payment->status != "succeeded")
            return false;

        return true;
    }

    private function setPaid(){
        if ($this->is_success == false) {
            $this->is_success = true;
            $this->save();
        }
    }

    private function isPaySberbank()
    {
        $pay = new SberbankPay();
        return $pay->payStatus($this->pay_id);
    }
}
