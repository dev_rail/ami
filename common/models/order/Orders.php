<?php

namespace common\models\order;

use backend\models\order\OrderStatus;
use common\components\SmsHelper;
use frontend\models\order\OrderStatuses;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "Orders".
 *
 * @property int $id
 * @property int $created_at
 * @property string $sum
 *
 * @property OrderCustomer $orderCustomer
 * @property OrderItems[] $orderItems
 * @property OrderStatuses[] $orderStatuses
 * @property int $type_pay [int(11)]
 * @property bool $paid [tinyint(1)]
 * @property int $finished_at [int(11)]
 * @property string $description [varchar(255)]
 * @property int $status [int(11)]
 */
class Orders extends \yii\db\ActiveRecord
{
    const TYPE_PAY_CASH = 1;
    const TYPE_PAY_ELECTRON = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'type_pay', 'finished_at', 'status'], 'integer'],
            [['sum'], 'number'],
            [['paid'], 'boolean'],
            ['description', 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'sum' => 'Sum',
            'dateCreateText' => 'Дата создания',
            'description' => 'Коментарий'
        ];
    }

    public function init()
    {
        parent::init();
        Event::on(OrderItems::className(), OrderItems::EVENT_AFTER_DELETE, [$this, "updateSum"]);
        Event::on(OrderItems::className(), OrderItems::EVENT_AFTER_INSERT, [$this, "updateSum"]);
        Event::on(OrderItems::className(), OrderItems::EVENT_AFTER_UPDATE, [$this, "updateSum"]);
        Event::on(OrderItems::className(), OrderPromotion::EVENT_AFTER_DELETE, [$this, "updateSum"]);
        Event::on(OrderItems::className(), OrderPromotion::EVENT_AFTER_INSERT, [$this, "updateSum"]);
        Event::on(OrderItems::className(), OrderPromotion::EVENT_AFTER_UPDATE, [$this, "updateSum"]);

        $this->on(self::EVENT_AFTER_INSERT, [$this, "createOrderLink"]);
        $this->on(self::EVENT_AFTER_INSERT, [$this, "createOrderRaiting"]);
    }

    public function getTypePayText()
    {
        switch ($this->type_pay)
        {
            case self::TYPE_PAY_CASH:
                return "Наличными";
            case self::TYPE_PAY_ELECTRON:
                return "Картой онлайн";
        }
    }

    public function getOrderRaiting(){
        return $this->hasOne(OrderRaiting::class, ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderCustomer()
    {
        return $this->hasOne(OrderCustomer::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatuses()
    {
        return $this->hasMany(OrderStatuses::className(), ['order_id' => 'id']);
    }

    public function getOrderPromotion(){
        return $this->hasOne(OrderPromotion::class, ['order_id' => 'id']);
    }

    public function updateSum()
    {
        if ($this->status == \common\models\order\OrderStatuses::STATUS_COMPLETE)
            return;

        $this->sum = $this->getOrderItems()->sum('sum');
        $promotion = $this->orderPromotion;

        if ($promotion)
            $this->sum -= $promotion->sum_discount;

        $this->save();
    }

    public function getOrderPay()
    {
        return $this->hasOne(OrderPay::class, ['order_id' => 'id']);
    }

    public function getPaidOnline()
    {
        if (!$this->orderPay)
            return false;

        /** @var OrderPay $order_pay */
        $order_pay = $this->orderPay;

        return $order_pay->getPaidOnline();
    }

    public function getOrderLink()
    {
        return $this->hasOne(OrderLink::class, ['order_id' => 'id']);
    }

    public function createOrderLink()
    {
        $model = new OrderLink([
            'code' => uniqid(),
            'order_id' => $this->id
        ]);

        while (!$model->validate())
            $model->code = uniqid();

        $model->save();
    }

    public function createOrderRaiting(){
        $model = new OrderRaiting([
            'order_id' => $this->id
        ]);

        $model->save();
    }

    public function deleteOrderLink()
    {
        $order_link = $this->orderLink;
        if ($order_link)
            $order_link->delete();
    }

    public function sendSms()
    {
        $text = "Ваш заказ оформлен. Подробнее ".Url::to(['/l/'.$this->orderLink->code], true);

        $customer = $this->orderCustomer->customer;
        $phone = $customer->phone;

        $sms = new SmsHelper();
        $sms->send_sms($phone, $text, false);
    }

    public function sendRaitingSms()
    {
        /** @var OrderRaiting $order_raiting */
        $order_raiting = $this->orderRaiting;

        if (!$order_raiting)
            return;

        if ($order_raiting->is_send == true)
            return;

        if (!$this->orderLink) {
            $order_raiting->delete();
            return;
        }

        $text = "Мы хотим стать лучше. Пожалуйста, оцените нашу работу https://amipizza.ru/l/r/".$this->orderLink->code;

        $customer = $this->orderCustomer->customer;
        $phone = $customer->phone;

        $sms = new SmsHelper();
        $sms->send_sms($phone, $text, false);

        $order_raiting->is_send = true;
        $order_raiting->save(false);
    }

    public function sendTelegram()
    {
        $text = "Заказ №".$this->id."\r\n";

        foreach ($this->orderItems as $item)
        {
            $text .= $item->getGoodFullName()." - ".$item->count." - ".$item->price." - ".$item->sum." \r\n";
        }

        if ($this->orderPromotion){
            $text .= "Сумма: ".$this->getOrderItems()->sum('sum')." \r\n";
            $text .= "Скидка: ".$this->orderPromotion->sum_discount." \r\n";
        }

        $text .= "Итого: ".$this->sum." \r\n";

        $text .= "Тип оплаты: ".$this->typePayText." \r\n";

        $text .= $this->orderCustomer->customer->phone." \r\n";

        if ($this->description)
            $text .= "Коментарий: ".$this->description." \r\n";

        if ($this->orderCustomer->address_id) {
            $text .= $this->orderCustomer->customerAddress->name;
        }

        Yii::$app->telegram->sendMessage([
            'chat_id' => -273213862,
            'text' => $text,
            'reply_markup' => json_encode([
                'inline_keyboard'=>[
                    [
                        ['text'=>"В работе",'callback_data'=> "job-".$this->id],
                        ['text'=>"В доставке",'callback_data'=> "delivery-".$this->id],
                        ['text'=>"Доставлен",'callback_data'=> "finish-".$this->id],
                        ['text'=>"Отказ",'callback_data'=> "cancel-".$this->id]
                    ]
                ]
            ])
        ]);
    }

    public function getDateCreateText(){
        return date("d.m.Y H:i:s", $this->created_at);
    }

    public function getIsPayText(){
        if ($this->paid)
            return "Оплачен";

        return "Не оплачен";
    }

    public function getStatusText()
    {
        return ArrayHelper::getValue(OrderStatuses::$statusTexts, $this->status);
    }

    public function setCooking($from = null)
    {
        if ($this->status == OrderStatus::STATUS_NEW)
        {
            $status = new OrderStatus([
                'status' => OrderStatus::STATUS_COOKING,
                'date' => time(),
                'telegram_user' => $from
            ]);

            $this->link('orderStatuses', $status);
        }
    }

    public function setDelivering($from = null)
    {
        if ($this->status == OrderStatus::STATUS_COOKING)
        {
            $status = new OrderStatus([
                'status' => OrderStatus::STATUS_DELIVERING,
                'date' => time(),
                'telegram_user' => $from
            ]);

            $this->link('orderStatuses', $status);
        }
    }

    public function setFinish($from = null)
    {
        if ($this->status != OrderStatus::STATUS_COMPLETE && $this->status != OrderStatus::STATUS_CANCEL) {
            $status = new OrderStatus([
                'status' => OrderStatus::STATUS_COMPLETE,
                'date' => time(),
                'telegram_user' => $from
            ]);

            $this->link('orderStatuses', $status);
            $this->paid = true;
            $this->finished_at = time();
            $this->save();
        }
    }

    public function setCancel($from = null){
        if ($this->status != OrderStatus::STATUS_CANCEL){
            $status = new OrderStatus([
                'status' => OrderStatus::STATUS_CANCEL,
                'date' => time(),
                'telegram_user' => $from
            ]);

            $this->link('orderStatuses', $status);
            $this->paid = false;
            $this->finished_at = time();
            $this->save();
        }
    }

    public function beforeDelete()
    {
        if ($this->orderCustomer)
            $this->orderCustomer->delete();

        foreach ($this->orderItems as $orderItem)
            $orderItem->delete();

        foreach ($this->orderStatuses as $orderStatus)
            $orderStatus->delete();

        return parent::beforeDelete();
    }
}
