<?php

namespace common\models\order;

use common\models\geo\Addresses;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "OrderStatuses".
 *
 * @property int $id
 * @property int $order_id
 * @property int $date
 * @property int $status
 * @property string $telegram_user
 *
 * @property Orders $order
 */
class OrderStatuses extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_COOKING = 10;
    const STATUS_DELIVERING = 20;
    const STATUS_COMPLETE = 30;
    const STATUS_CANCEL = 100;

    public static $statusTexts = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_COOKING => 'Готовится',
        self::STATUS_DELIVERING => 'В доставке',
        self::STATUS_COMPLETE => 'Завершен',
        self::STATUS_CANCEL => 'Отменен'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OrderStatuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'date', 'status'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            ['telegram_user', 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    public function getStatusText()
    {
        return self::$statusTexts[$this->status];
    }

    private function setOrderStatus()
    {
        $order = $this->order;
        $order->status = $this->status;
        $order->save();
    }

    public function beforeSave($insert)
    {
        if ($insert)
            $this->sendTelegram();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->setOrderStatus();
    }

    private function sendTelegram()
    {
        if ($this->status == self::STATUS_NEW)
            return;

        $text = "Заказ № ".$this->order_id." ";

        $text .= static::$statusTexts[$this->status];

        if ($this->status == self::STATUS_DELIVERING) {
            /** @var Addresses $address */
            $address = $this->order->orderCustomer->address;
            if ($address) {
                $location = $address->getLocation();
                $lat = ArrayHelper::getValue($location, "lat");
                $lon = ArrayHelper::getValue($location, "lon");

                $text .= " <a href='https://amipizza.ru/bot/navigate?lat=".$lat."&lon=".$lon."'>Навигатор</a>";
            }
        }

        Yii::$app->telegram->sendMessage([
            'chat_id' => -273213862,
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);
    }
}
