<?php

namespace common\models\order;

use Yii;

/**
 * This is the model class for table "OrderRaiting".
 *
 * @property int $id
 * @property int $order_id
 * @property string $raiting_product
 * @property string $raiting_delivery
 * @property string $description
 * @property int $is_send
 * @property int $is_processed
 *
 * @property Orders $order
 */
class OrderRaiting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OrderRaiting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['is_send', 'is_processed'], 'boolean'],
            [['raiting_product', 'raiting_delivery'], 'number'],
            [['description'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'raiting_product' => 'Пицца',
            'raiting_delivery' => 'Доставка',
            'description' => 'Коментарий',
            'is_send' => 'Is Send',
            'is_processed' => 'Is Processed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }
}
