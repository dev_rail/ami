<?php

namespace common\models\order;

use common\models\good\Goods;
use common\models\good\GoodSizes;
use Yii;

/**
 * This is the model class for table "Cart".
 *
 * @property int $id
 * @property string $session_id
 * @property int $good_id
 * @property int $good_size_id
 * @property string $count
 * @property number $price
 * @property number $sum
 *
 * @property Goods $good
 * @property GoodSizes $goodSize
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Cart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['good_id', 'good_size_id'], 'integer'],
            [['count', 'sum', 'price'], 'number'],
            [['session_id'], 'string', 'max' => 255],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['good_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => GoodSizes::className(), 'targetAttribute' => ['good_size_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session_id' => 'Session ID',
            'good_id' => 'Good ID',
            'good_size_id' => 'Good Size ID',
            'count' => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Goods::className(), ['id' => 'good_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodSize()
    {
        return $this->hasOne(GoodSizes::className(), ['id' => 'good_size_id']);
    }

    public function getPrice()
    {
        if ($this->good_size_id)
            return $this->goodSize->price;

        return $this->good->price;
    }

    public function beforeSave($insert)
    {
        if ($insert)
            $this->price = $this->getPrice();

        $this->updateSum();

        return parent::beforeSave($insert);
    }

    public function updateSum()
    {
        $this->sum = $this->price * $this->count;
    }
}
