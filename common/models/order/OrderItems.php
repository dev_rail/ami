<?php

namespace common\models\order;

use common\models\good\Goods;
use common\models\good\GoodSizes;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "OrderItems".
 *
 * @property int $id
 * @property int $order_id
 * @property int $good_id
 * @property int $count
 * @property string $price
 * @property string $sum
 *
 * @property Goods $good
 * @property Orders $order
 * @property int $good_size_id [int(11)]
 */
class OrderItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OrderItems';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'good_id', 'count', 'good_size_id'], 'integer'],
            [['price', 'sum'], 'number'],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['good_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => GoodSizes::className(), 'targetAttribute' => ['good_size_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'good_id' => 'Good ID',
            'count' => 'Количество',
            'price' => 'Цена',
            'sum' => 'Сумма',
            'goodFullName' => 'Наименование'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Goods::className(), ['id' => 'good_id']);
    }

    public function getGoodSize()
    {
        return $this->hasOne(GoodSizes::class, ['id' => 'good_size_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    private function updateSum()
    {
        $this->sum = $this->count * $this->price;
    }

    public function beforeSave($insert)
    {
        $this->updateSum();
        return parent::beforeSave($insert);
    }

    public function getGoodFullName()
    {
        $name = $this->good->name;

        if ($this->goodSize)
            $name .= " ".ArrayHelper::getValue($this, "goodSize.categorySize.name");

        if ($this->good->is_combo)
            $name .= " ".$this->good->description;

        return $name;
    }
}
