<?php

namespace common\models\order;

use common\models\promotions\Promotions;
use Yii;

/**
 * This is the model class for table "OrderPromotion".
 *
 * @property int $id
 * @property int $order_id
 * @property int $promotion_id
 * @property string $sum_discount
 *
 * @property Orders $order
 * @property Promotions $promotion
 */
class OrderPromotion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'OrderPromotion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['order_id', 'unique'],
            [['order_id', 'promotion_id'], 'integer'],
            [['sum_discount'], 'number'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['promotion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Promotions::className(), 'targetAttribute' => ['promotion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'promotion_id' => 'Promotion ID',
            'sum_discount' => 'Sum Discount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromotion()
    {
        return $this->hasOne(Promotions::className(), ['id' => 'promotion_id']);
    }
}
