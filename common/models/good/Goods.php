<?php

namespace common\models\good;

use common\models\order\OrderItems;
use Yii;

/**
 * This is the model class for table "Goods".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property boolean $is_combo
 *
 * @property Compositions[] $compositions
 * @property GoodPhotos[] $goodPhotos
 * @property OrderItems[] $orderItems
 */
class Goods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['sort', 'integer'],
            [['price'], 'number'],
            [['name', 'description'], 'string', 'max' => 255],
            ['sort', 'default', 'value' => 1],
            ['is_combo', 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'price' => 'Цена',
            'sort' => 'Сортировка',
            'is_combo' => "Комбо"
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompositions()
    {
        return $this->hasMany(Compositions::className(), ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodPhotos()
    {
        return $this->hasMany(GoodPhotos::className(), ['good_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(), ['good_id' => 'id']);
    }

    public function getCategoryGood()
    {
        return $this->hasOne(CategoryGood::class, ['good_id' => 'id']);
    }

    public function getGoodSizes()
    {
        return $this->hasMany(GoodSizes::class, ['good_id' => 'id']);
    }

    public function beforeDelete()
    {
        if ($this->categoryGood)
            $this->categoryGood->delete();

        if ($this->goodPhotos)
            foreach ($this->goodPhotos as $goodPhoto)
                $goodPhoto->delete();

        return parent::beforeDelete();
    }
}
