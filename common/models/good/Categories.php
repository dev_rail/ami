<?php

namespace common\models\good;

use Yii;

/**
 * This is the model class for table "Categories".
 *
 * @property int $id
 * @property string $name
 * @property string $icon
 *
 * @property CategoryGood[] $categoryGoods
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'icon' => 'Иконка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryGoods()
    {
        return $this->hasMany(CategoryGood::className(), ['category_id' => 'id']);
    }
}
