<?php

namespace common\models\good;

use Yii;

/**
 * This is the model class for table "GoodSizes".
 *
 * @property int $id
 * @property int $good_id
 * @property int $category_size_id
 * @property string $price
 * @property integer $weight
 *
 * @property CategorySizes $categorySize
 * @property Goods $good
 */
class GoodSizes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'GoodSizes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['good_id', 'category_size_id', 'weight'], 'integer'],
            [['price'], 'number'],
            [['category_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategorySizes::className(), 'targetAttribute' => ['category_size_id' => 'id']],
            [['good_id'], 'exist', 'skipOnError' => true, 'targetClass' => Goods::className(), 'targetAttribute' => ['good_id' => 'id']],
            [['good_id', 'category_size_id'], 'unique', 'targetAttribute' => ['good_id', 'category_size_id'], 'message' => 'Данный размер уже добавлен!']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'good_id' => 'Good ID',
            'category_size_id' => 'Размер',
            'price' => 'Цена',
            'weight' => 'Вес'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorySize()
    {
        return $this->hasOne(CategorySizes::className(), ['id' => 'category_size_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Goods::className(), ['id' => 'good_id']);
    }
}
