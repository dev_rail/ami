<?php

namespace common\models\good;

use Yii;

/**
 * This is the model class for table "CategorySizes".
 *
 * @property int $id
 * @property int $category_id
 * @property string $size
 * @property string $unit
 *
 * @property Categories $category
 */
class CategorySizes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CategorySizes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['size'], 'number'],
            [['unit'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'size' => 'Размер',
            'unit' => 'Единица измерения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function getName()
    {
        return $this->size." ".$this->unit;
    }
}
