<?php

namespace common\models\geo;

use common\models\customer\CustomerAddresses;
use common\models\Objects;
use common\models\order\OrderCustomer;
use Yii;

/**
 * This is the model class for table "Addresses".
 *
 * @property int $id
 * @property string $address
 * @property string $lat
 * @property string $lon
 *
 * @property CustomerAddresses[] $customerAddresses
 * @property Objects[] $objects
 * @property OrderCustomer[] $orderCustomers
 */
class Addresses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Addresses';
    }

    public static function getAddress($address)
    {
        $model = static::findOne(['address' => $address]);

        if (!$model)
        {
            $model = new static(['address' => $address]);
            $model->save();
        }

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lat', 'lon'], 'number'],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'lat' => 'Lat',
            'lon' => 'Lon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerAddresses()
    {
        return $this->hasMany(CustomerAddresses::className(), ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Objects::className(), ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderCustomers()
    {
        return $this->hasMany(OrderCustomer::className(), ['address_id' => 'id']);
    }

    public function getLocation(){
        if (!$this->lat or !$this->lon){
            $location = Yii::$app->geo->getLatLng($this->address);
            $this->lat = $location["lat"];
            $this->lon = $location["lng"];
            $this->save();
        }

        return [
            'lat' => $this->lat,
            'lon' => $this->lon
        ];
    }
}
