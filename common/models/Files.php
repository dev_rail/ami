<?php

namespace common\models;

use common\models\good\GoodPhotos;
use Yii;

/**
 * This is the model class for table "Files".
 *
 * @property int $id
 * @property string $src
 *
 * @property GoodPhotos[] $goodPhotos
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['src'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'src' => 'Src',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodPhotos()
    {
        return $this->hasMany(GoodPhotos::className(), ['file_id' => 'id']);
    }

    public function beforeDelete()
    {
        if ($this->src)
            unlink(Yii::getAlias('@common/web').$this->src);

        return parent::beforeDelete();
    }
}
