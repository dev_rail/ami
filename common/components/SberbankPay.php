<?php

namespace common\components;

use common\models\order\OrderPay;
use common\models\settings\PaySettings;
use frontend\models\order\Order;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\httpclient\Client;

class SberbankPay extends Component
{
    private $url = "https://3dsec.sberbank.ru/payment/rest/";
    private $client;

    public function init()
    {
        parent::init();
        $this->client = new Client();
        $this->client->baseUrl = $this->url;
    }

    public function pay(Order $order){
        /** @var Client $client */
        $client = $this->client;

        if ($order->orderPay)
            return $order->orderPay->url;

        $request = $client->post('register.do', [
            'userName' => \Yii::$app->params['sberbank']['pay']['login'],
            'password' => \Yii::$app->params['sberbank']['pay']['password'],
            'orderNumber' => $order->id,
            'amount' => $order->sum * 100,
            'returnUrl' => \Yii::$app->params['domens']['ami']."/order/sberbank"
        ]);


        $response = $request->send();
        $pay_id = ArrayHelper::getValue($response->data, "orderId");
        $url = ArrayHelper::getValue($response->data, "formUrl");

        $order->addPay($pay_id, $url, PaySettings::TYPE_SBERBANK);

        return $url;
    }

    public function payStatus($pay_id){
        /** @var Client $client */
        $client = $this->client;

        $order_pay = OrderPay::findOne(['pay_id' => $pay_id, 'type' => PaySettings::TYPE_SBERBANK]);

        $request = $client->post('getOrderStatusExtended.do', [
            'userName' => \Yii::$app->params['sberbank']['pay']['login'],
            'password' => \Yii::$app->params['sberbank']['pay']['password'],
            'orderId' => $pay_id,
            'orderNumber' => $order_pay->order_id
        ]);

        $response = $request->send();
        $data = $response->data;

        $order_status = ArrayHelper::getValue($data, "orderStatus");

        if ($order_status == 2)
            return true;

        return false;
    }
}