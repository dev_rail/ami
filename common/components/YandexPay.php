<?php


namespace common\components;


use frontend\models\order\Order;
use YandexCheckout\Client;
use yii\base\Component;
use yii\helpers\Url;

class YandexPay extends Component
{
    /** @var Client $client */
    public $client;

    public function init()
    {
        parent::init();
        $this->client = new Client();
        $this->client->setAuth(\Yii::$app->params['yandex']['pay']['shop_id'], \Yii::$app->params['yandex']['pay']['key']);
    }

    public function pay(Order $order)
    {
        $idempotenceKey = uniqid('', true);
        $response = $this->client->createPayment(
            array(
                'amount' => array(
                    'value' => $order->sum,
                    'currency' => 'RUB',
                ),
                'payment_method_data' => array(
                    'type' => 'bank_card',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => Url::to(['/order/view', 'id' => $order->id], true),
                ),
                'capture' => true,
                'description' => 'Заказ №'.$order->id,
            ),
            $idempotenceKey
        );

        $order->addPay($response->id);

        return $response->confirmation->confirmation_url;
    }

    public function getStatus($pay_id)
    {
        $payment = $this->client->getPaymentInfo($pay_id);

        return $payment;
    }
}
