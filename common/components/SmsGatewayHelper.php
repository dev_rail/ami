<?php


namespace common\components;

use Exception;
use SMSGatewayMe\Client\Api\MessageApi;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Model\SendMessageRequest;
use yii\base\Component;

class SmsGatewayHelper extends Component
{
    public $instance;
    public $uid;

    public function init()
    {
        parent::init();
        Configuration::getDefaultConfiguration()->setApiKey('Authorization', \Yii::$app->params['smsgateway_api']);
        $this->instance = new MessageApi();
    }

    public function send($phone, $message)
    {
        $api_message = new SendMessageRequest(['phoneNumber' => $phone, 'message' => $message, 'deviceId' => $this->uid]);

        try {
            $result = $this->instance->sendMessages(array($api_message));
        } catch (Exception $e) {
            echo 'Exception when calling MessageApi->sendMessages: ', $e->getMessage(), "\n";
        }
    }
}