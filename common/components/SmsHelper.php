<?php


namespace common\components;


use common\models\settings\SmsSettings;
use yii\base\Component;

class SmsHelper extends Component
{
    public function send_sms($phones, $message)
    {
        $model = SmsSettings::find()->one();
        if (!$model)
            return;

        switch ($model->type){
            case SmsSettings::TYPE_SMSC:
                return $this->send_smsc($phones, $message);
            case SmsSettings::TYPE_SMSGETWAY:
                return $this->send_sms_getway($model->smsGetWay->uid, $phones, $message);
        }
    }

    private function send_sms_getway($uid, $phones, $message)
    {
        if ($uid == false)
            return;

        $message .= "\r\n"."https://amipizza.ru";
        $sms = new SmsGatewayHelper([
            'uid' => $uid
        ]);
        $sms->send($phones, $message);
    }

    private function send_smsc($phones, $message){
        $sms = new SmsCHelper();
        $sms->send_sms($phones, $message);
    }
}
