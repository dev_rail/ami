<?php

namespace common\components;


use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

class Geo extends Component
{
    public $key;
    private $url = "https://maps.googleapis.com/maps/api/geocode/json";

    public function getLatLng($address){
        $client = new Client();
        $response = $client->createRequest()
            ->setUrl($this->url)
            ->setMethod('GET')
            ->setData([
                'address' => $address,
                'key' => $this->key
            ])
            ->send();

        if ($response->isOk)
            return ArrayHelper::getValue($response->data, "results.0.geometry.location");

        return [];
    }
}